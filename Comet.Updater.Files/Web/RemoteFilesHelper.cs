﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Files - RemoteFilesHelper.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 15:33
// ===================================================================

#endregion

#region References

using System;
using System.Net;
using System.Threading.Tasks;

#endregion

namespace Comet.Updater.Files.Web
{
    public static class RemoteFilesHelper
    {
        private const double _KBYTE = 1024;
        private const double _MBYTE = _KBYTE * 1024;
        private const double _GBYTE = _MBYTE * 1024;

        public static bool FileExists(string url)
        {
            HttpWebResponse response = null;
            var request = (HttpWebRequest) WebRequest.Create(url);
            request.Method = "HEAD";
            request.UserAgent = "World-Conquer-Online-Auto-Patcher";
            try
            {
                response = (HttpWebResponse) request.GetResponse();
                return true;
            }
            catch (WebException)
            {
                /* A WebException will be thrown if the status of the response is not `200 OK` */
                return false;
            }
            finally
            {
                // Don't forget to close your response.
                response?.Close();
            }
        }

        public static async Task<bool> FileExistAsync(string url)
        {
            HttpWebResponse response = null;
            var request = (HttpWebRequest) WebRequest.Create(url);
            request.Method = "HEAD";
            request.UserAgent = "World-Conquer-Online-Auto-Patcher";
            try
            {
                response = (HttpWebResponse) await request.GetResponseAsync();
                return true;
            }
            catch (WebException)
            {
                /* A WebException will be thrown if the status of the response is not `200 OK` */
                return false;
            }
            finally
            {
                // Don't forget to close your response.
                response?.Close();
            }
        }

        public static string ReadStringFromUrl(string url)
        {
            if (!FileExists(url))
                return null;

            try
            {
                using var web = new WebClient();
                return web.DownloadString(url);
            }
            catch
            {
                return null;
            }
        }

        public static async Task<string> ReadStringFromUrlAsync(string url)
        {
            if (!FileExists(url))
                return null;

            try
            {
                using var web = new WebClient();
                return await web.DownloadStringTaskAsync(new Uri(url));
            }
            catch
            {
                return string.Empty;
            }
        }

        public static long FetchFileSize(string url)
        {
            if (!FileExists(url))
                return 0;

            WebRequest req = WebRequest.Create(url);
            req.Method = "HEAD";
            req.Timeout = 2500;
            WebResponse resp = null;
            try
            {
                resp = req.GetResponse();
                if (long.TryParse(resp.Headers.Get("Content-Length"), out var contentLength))
                {
                    return contentLength;
                }
            }
            catch
            {
                return 0;
            }
            finally
            {
                resp?.Close();
            }

            return 0;
        }

        public static async Task<long> FetchFileSizeAsync(string url)
        {
            WebRequest req = WebRequest.Create(url);
            req.Method = "HEAD";
            req.Timeout = 2500;
            WebResponse resp = null;
            try
            {
                resp = await req.GetResponseAsync();
                if (long.TryParse(resp.Headers.Get("Content-Length"), out var contentLength))
                {
                    return contentLength;
                }
            }
            catch
            {
                return -1;
            }
            finally
            {
                resp?.Close();
            }

            return -1;
        }

        public static string ParseFileSize(long size)
        {
            if (size > _GBYTE)
                return $"{size / _GBYTE:N2} GB";
            if (size > _MBYTE)
                return $"{size / _MBYTE:N2} MB";
            if (size > _KBYTE)
                return $"{size / _KBYTE:N2} KB";
            return $"{size} B";
        }

        public static string ParseDownloadSpeed(long amount)
        {
            if (amount > _GBYTE)
                return $"{amount / _GBYTE:N2} GB/s";
            if (amount > _MBYTE)
                return $"{amount / _MBYTE:N2} MB/s";
            if (amount > _KBYTE)
                return $"{amount / _KBYTE:N2} KB/s";
            return $"{amount} B/s";
        }
    }
}
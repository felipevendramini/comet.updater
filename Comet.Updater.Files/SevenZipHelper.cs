﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Files - SevenZipHelper.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/24 18:05
// ===================================================================

#endregion

#region References

using System.IO;
using ManagedLzma;
using ManagedLzma.SevenZip.FileModel;
using ManagedLzma.SevenZip.Reader;

#endregion

namespace Comet.Updater.Files
{
    public static class SevenZipHelper
    {
        public static void Extract7ZipFile(string zipFile, string targetPath)
        {
            var file = new FileStream(zipFile, FileMode.Open, FileAccess.Read, FileShare.Read);
            var mdReader = new ArchiveFileModelMetadataReader();
            // enables an unofficial extension; should be safe to always set to true if you need to deal with those kind of files
            var mdModel = mdReader.ReadMetadata(file);

            for (int sectionIndex = 0; sectionIndex < mdModel.Metadata.DecoderSections.Length; sectionIndex++)
            {
                var dsReader =
                    new DecodedSectionReader(file, mdModel.Metadata, sectionIndex, PasswordStorage.Create(""));
                var mdFiles = mdModel.GetFilesInSection(sectionIndex);

                while (dsReader.CurrentStreamIndex < dsReader.StreamCount)
                {
                    var mdFile = mdFiles[dsReader.CurrentStreamIndex];
                    if (mdFile != null)
                    {
                        string outputPath = Path.Combine(targetPath, mdFile.FullName);

                        if (File.Exists(outputPath))
                            File.Delete(outputPath);

                        using FileStream outputFile = File.Create(outputPath);
                        var substream = dsReader.OpenStream();
                        if (mdFile.Offset != 0)
                            return;
                        substream.CopyTo(outputFile);
                        outputFile.Close();
                    }

                    dsReader.NextStream();
                }
            }
        }
    }
}
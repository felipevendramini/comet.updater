﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Files - DirectoryListener.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/12 15:09
// ===================================================================

#endregion

#region References

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

#endregion

namespace Comet.Updater.Files
{
    public sealed class DirectoryListener
    {
        private List<FileSystemWatcher> mListeners;

        private DirectoryListener(FileSystemEventHandler onCreate = null, FileSystemEventHandler onChange = null, FileSystemEventHandler onDelete = null, RenamedEventHandler onRename = null)
        {
            mListeners = new List<FileSystemWatcher>();

            OnChange = onChange;
            OnCreate = onCreate;
            OnDelete = onDelete;
            OnRename = onRename;
        }

        public event FileSystemEventHandler OnCreate;
        public event FileSystemEventHandler OnChange;
        public event FileSystemEventHandler OnDelete;
        public event RenamedEventHandler OnRename;

        public static DirectoryListener Create(string[] directories, FileSystemEventHandler onCreate = null, FileSystemEventHandler onChange = null,
            FileSystemEventHandler onDelete = null, RenamedEventHandler onRename = null)
        {
            DirectoryListener listener = new DirectoryListener(onCreate, onChange, onDelete);

            foreach (var dir in directories)
            {
                FileSystemWatcher watcher = new FileSystemWatcher(dir)
                {
                    IncludeSubdirectories = true,
                    NotifyFilter = NotifyFilters.Attributes
                                   | NotifyFilters.CreationTime
                                   | NotifyFilters.DirectoryName
                                   | NotifyFilters.FileName
                                   | NotifyFilters.LastAccess
                                   | NotifyFilters.LastWrite
                                   | NotifyFilters.Security
                                   | NotifyFilters.Size
                };
                if (listener.OnCreate != null)
                    watcher.Created += listener.OnCreate;
                if (listener.OnChange != null)
                    watcher.Changed += listener.OnChange;
                if (listener.OnDelete != null)
                    watcher.Deleted += listener.OnDelete;
                if (listener.OnRename != null)
                    watcher.Renamed += listener.OnRename;

                listener.mListeners.Add(watcher);
            }

            return listener;
        }

        public void StartListening()
        {
            foreach (var listener in mListeners)
            {
                listener.EnableRaisingEvents = true;
            }
        }

        public void StopListening()
        {
            foreach (var listener in mListeners)
            {
                listener.EnableRaisingEvents = false;
            }
        }
    }
}
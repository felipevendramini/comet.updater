﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - UserConfigurationManager.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/23 11:43
// ===================================================================

#endregion

#region References

using System;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Client.Configuration
{
    public sealed class UserConfigurationManager
    {
        public const string FILE_S = "AutoPatchUserConfig.xml";

        public static UserConfigurationManager Configuration = null;

        public const int MINIMUM_FPS_I = 30;
        public const int DEFAULT_FPS_I = 40;
        public const int MAXIMUM_FPS_I = 120;

        public const int MINIMUM_WIDTH_I = 1024;
        public const int MINIMUM_HEIGHT_I = 768;
        
        private XmlParser mXmlParser = null;

        private int mFramesPerSecond;
        private int mScreenWidth;
        private int mScreenHeight;
        private DateTime? mPowerShellWindowsDefender;

        public bool Initialize()
        {
            if (!File.Exists(FILE_S))
            {
                mFramesPerSecond = DEFAULT_FPS_I;
                mScreenWidth = MINIMUM_WIDTH_I;
                mScreenHeight = MINIMUM_HEIGHT_I;
                return false;
            }

            try
            {
                mXmlParser = new XmlParser(FILE_S);

                mFramesPerSecond = int.Parse(mXmlParser.GetValue("Config", "FramesPerSecond"));
                mScreenWidth = int.Parse(mXmlParser.GetValue("Config", "Resolution", "Width"));
                mScreenHeight = int.Parse(mXmlParser.GetValue("Config", "Resolution", "Height"));

                FramesPerSecond = Math.Min(Math.Max(mFramesPerSecond, MINIMUM_FPS_I), MAXIMUM_FPS_I);
                ScreenWidth = Math.Max(1024, Math.Min(MaxScreenWidth, mScreenWidth));
                ScreenHeight = Math.Max(768, Math.Min(MaxScreenHeight, mScreenHeight));

                if (DateTime.TryParse(mXmlParser.GetValue("Config", "PowerShellWindowsDefender"), out var powerShell))
                    mPowerShellWindowsDefender = powerShell;

                Save();
            }
            catch
            {
                // Delete the file so we can guarantee to load a valid one next time.
                File.Delete(FILE_S);

                mFramesPerSecond = DEFAULT_FPS_I;
                mScreenWidth = MINIMUM_WIDTH_I;
                mScreenHeight = MINIMUM_HEIGHT_I;

                return false;
            }
            return true;
        }

        public int FramesPerSecond
        {
            get => mFramesPerSecond;
            set
            {
                mFramesPerSecond = Math.Min(Math.Max(value, MINIMUM_FPS_I), MAXIMUM_FPS_I);
                mXmlParser.ChangeValue(mFramesPerSecond.ToString(CultureInfo.InvariantCulture), "Config", "FramesPerSecond");
            }
        }

        public int ScreenWidth
        {
            get => mScreenWidth;
            set
            {
                mScreenWidth = Math.Max(MINIMUM_WIDTH_I, Math.Min(MaxScreenWidth, value));
                mXmlParser.ChangeValue(mScreenWidth.ToString(CultureInfo.InvariantCulture), "Config", "Resolution", "Width");
            }
        }

        public int ScreenHeight
        {
            get => mScreenHeight;
            set
            {
                mScreenHeight = Math.Max(MINIMUM_HEIGHT_I, Math.Min(MaxScreenHeight, value));
                mXmlParser.ChangeValue(mScreenHeight.ToString(CultureInfo.InvariantCulture), "Config", "Resolution", "Height");
            }
        }

        public int MaxScreenWidth => Screen.PrimaryScreen.Bounds.Width;
        public int MaxScreenHeight => Screen.PrimaryScreen.Bounds.Height;

        public DateTime? PowerShellWindowsDefender
        {
            get => mPowerShellWindowsDefender;
            set
            {
                mPowerShellWindowsDefender = value;
                mXmlParser.AddNewNode(value?.ToString("O") ?? "", null, "PowerShellWindowsDefender", "Config");
            }
        }

        public void Save()
        {
            mXmlParser?.Save();
        }
    }
}
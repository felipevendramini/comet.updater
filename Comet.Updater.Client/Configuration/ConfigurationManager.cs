﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - ConfigurationManager.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/02/05 13:01
// ===================================================================

#endregion

#region References

using System;
using System.Collections.Generic;
using System.IO;
using Comet.Updater.Files.Web;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Client.Configuration
{
    public class ConfigurationManager
    {
        public const string FILE_S = "AutoPatch.xml";

        public ConfigurationManager()
        {
            var xmlParser = new XmlParser(Path.Combine(Environment.CurrentDirectory, FILE_S));
            int addressNum = int.Parse(xmlParser.GetValue("Config", "AddressesNum"));

            ConnectAddress = new List<ConnectAddressStruct>();

            ConnectAddressOnline = xmlParser.GetValue("Config", "ConnectAddressOnline");

            if (!string.IsNullOrEmpty(ConnectAddressOnline))
            {
                string remote = RemoteFilesHelper.ReadStringFromUrl(ConnectAddressOnline).Trim('\n');
                if (!string.IsNullOrEmpty(remote))
                    ConnectAddress.Add(new ConnectAddressStruct(remote));
            }

            for (int i = 0; i < addressNum; i++)
            {
                string value = xmlParser.GetValue("Config", "Addresses", $"Address{i}");
                if (string.IsNullOrEmpty(value))
                {
                    Log.WriteLogAsync(LogLevel.Warning, $"Error Parsing Address{i} {value}").ConfigureAwait(false);
                    continue;
                }

                ConnectAddress.Add(new ConnectAddressStruct(value));
            }

            if (DateTime.TryParse(xmlParser.GetValue("Config", "TermsOfPrivacy"), out var privacy))
                TermsOfPrivacy = privacy;
        }

        public List<ConnectAddressStruct> ConnectAddress { get; set; }
        public string ConnectAddressOnline { get; set; }

        public DateTime? TermsOfPrivacy { get; set; }

        public void SetTermsOfPrivacy()
        {
            var xmlParser = new XmlParser(Path.Combine(Environment.CurrentDirectory, FILE_S));
            xmlParser.AddNewNode(DateTime.Now.ToString("O"), null, "TermsOfPrivacy", "Config");
            xmlParser.Save();
        }

        public struct ConnectAddressStruct
        {
            public ConnectAddressStruct(string full)
            {
                IpAddress = full.Split(':')[0];
                Port = int.Parse(full.Split(':')[1]);
            }

            public string IpAddress;
            public int Port;
        }
    }
}
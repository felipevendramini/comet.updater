﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - MessagePipe.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/02/02 16:35
// ===================================================================

#endregion

using System;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Comet.Updater.Client
{
    public sealed class MessagePipe
    {
        protected readonly Channel<Func<Task>>[] Channels; 

        public MessagePipe()
        {
            
        }
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - BaseThread.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 19:44
// ===================================================================

#endregion

#region References

using System;
using System.Threading.Tasks;
using Comet.Updater.Client.Communication;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Client.Threads
{
    public sealed class BaseThread : TimerBase
    {
        private int mLastConnectionAttempt;
        
        public BaseThread()
            : base(1000, "BaseThread")
        {
        }

        protected override async Task<bool> OnElapseAsync()
        {
            if (Kernel.Socket == null)
            {
                if ((Environment.TickCount - mLastConnectionAttempt) > 10000)
                {
                    var socket = new ClientSocket();
                    foreach (var config in Kernel.Configuration.ConnectAddress)
                    {
                        if (await socket.ConnectToAsync(config.IpAddress, config.Port))
                        {
                            Kernel.Socket = socket;

                            Kernel.MainForm.SetProgressStatus(false);
                            Kernel.MainForm.SetStatusLabel(LanguageManager.GetString("StrConnectedToServer"));
                            break;
                        }
                    }

                    mLastConnectionAttempt = Environment.TickCount;
                }
            }
            else
            {
                
            }

            return true;
        }
    }
}
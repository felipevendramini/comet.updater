﻿
namespace Comet.Updater.Client
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFps = new System.Windows.Forms.Label();
            this.numFps = new System.Windows.Forms.NumericUpDown();
            this.lnkDefaultFps = new System.Windows.Forms.LinkLabel();
            this.lblScreenResolution = new System.Windows.Forms.Label();
            this.numScreenWidth = new System.Windows.Forms.NumericUpDown();
            this.numScreenHeight = new System.Windows.Forms.NumericUpDown();
            this.lnkDefaultResolution = new System.Windows.Forms.LinkLabel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAccept = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numFps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScreenWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScreenHeight)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFps
            // 
            this.lblFps.AutoSize = true;
            this.lblFps.BackColor = System.Drawing.Color.Transparent;
            this.lblFps.Location = new System.Drawing.Point(25, 24);
            this.lblFps.Name = "lblFps";
            this.lblFps.Size = new System.Drawing.Size(39, 15);
            this.lblFps.TabIndex = 0;
            this.lblFps.Text = "StrFps";
            // 
            // numFps
            // 
            this.numFps.Location = new System.Drawing.Point(118, 22);
            this.numFps.Maximum = new decimal(new int[] {
            144,
            0,
            0,
            0});
            this.numFps.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numFps.Name = "numFps";
            this.numFps.Size = new System.Drawing.Size(62, 23);
            this.numFps.TabIndex = 0;
            this.numFps.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // lnkDefaultFps
            // 
            this.lnkDefaultFps.AutoSize = true;
            this.lnkDefaultFps.BackColor = System.Drawing.Color.Transparent;
            this.lnkDefaultFps.Location = new System.Drawing.Point(186, 24);
            this.lnkDefaultFps.Name = "lnkDefaultFps";
            this.lnkDefaultFps.Size = new System.Drawing.Size(71, 15);
            this.lnkDefaultFps.TabIndex = 1;
            this.lnkDefaultFps.TabStop = true;
            this.lnkDefaultFps.Text = "StrToDefault";
            this.lnkDefaultFps.VisitedLinkColor = System.Drawing.Color.Blue;
            this.lnkDefaultFps.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkDefaultFps_LinkClicked);
            // 
            // lblScreenResolution
            // 
            this.lblScreenResolution.AutoSize = true;
            this.lblScreenResolution.BackColor = System.Drawing.Color.Transparent;
            this.lblScreenResolution.Location = new System.Drawing.Point(26, 53);
            this.lblScreenResolution.Name = "lblScreenResolution";
            this.lblScreenResolution.Size = new System.Drawing.Size(76, 15);
            this.lblScreenResolution.TabIndex = 3;
            this.lblScreenResolution.Text = "StrScreenSize";
            // 
            // numScreenWidth
            // 
            this.numScreenWidth.Location = new System.Drawing.Point(118, 51);
            this.numScreenWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numScreenWidth.Minimum = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.numScreenWidth.Name = "numScreenWidth";
            this.numScreenWidth.Size = new System.Drawing.Size(62, 23);
            this.numScreenWidth.TabIndex = 2;
            this.numScreenWidth.Value = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            // 
            // numScreenHeight
            // 
            this.numScreenHeight.Location = new System.Drawing.Point(186, 51);
            this.numScreenHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numScreenHeight.Minimum = new decimal(new int[] {
            768,
            0,
            0,
            0});
            this.numScreenHeight.Name = "numScreenHeight";
            this.numScreenHeight.Size = new System.Drawing.Size(62, 23);
            this.numScreenHeight.TabIndex = 3;
            this.numScreenHeight.Value = new decimal(new int[] {
            768,
            0,
            0,
            0});
            // 
            // lnkDefaultResolution
            // 
            this.lnkDefaultResolution.AutoSize = true;
            this.lnkDefaultResolution.BackColor = System.Drawing.Color.Transparent;
            this.lnkDefaultResolution.Location = new System.Drawing.Point(254, 53);
            this.lnkDefaultResolution.Name = "lnkDefaultResolution";
            this.lnkDefaultResolution.Size = new System.Drawing.Size(71, 15);
            this.lnkDefaultResolution.TabIndex = 4;
            this.lnkDefaultResolution.TabStop = true;
            this.lnkDefaultResolution.Text = "StrToDefault";
            this.lnkDefaultResolution.VisitedLinkColor = System.Drawing.Color.Blue;
            this.lnkDefaultResolution.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkDefaultResolution_LinkClicked);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BackgroundImage = global::Comet.Updater.Client.Properties.Resources.More;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("SimSun", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancel.Location = new System.Drawing.Point(284, 157);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 32);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "StrCancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnCancel_MouseDown);
            this.btnCancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCancel_MouseUp);
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.Transparent;
            this.btnAccept.BackgroundImage = global::Comet.Updater.Client.Properties.Resources.More;
            this.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAccept.FlatAppearance.BorderSize = 0;
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccept.Font = new System.Drawing.Font("SimSun", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAccept.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAccept.Location = new System.Drawing.Point(186, 157);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(94, 32);
            this.btnAccept.TabIndex = 5;
            this.btnAccept.Text = "StrSave";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            this.btnAccept.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnCancel_MouseDown);
            this.btnAccept.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCancel_MouseUp);
            // 
            // FrmSettings
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Comet.Updater.Client.Properties.Resources.Hint;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(399, 207);
            this.ControlBox = false;
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lnkDefaultResolution);
            this.Controls.Add(this.numScreenHeight);
            this.Controls.Add(this.numScreenWidth);
            this.Controls.Add(this.lblScreenResolution);
            this.Controls.Add(this.lnkDefaultFps);
            this.Controls.Add(this.numFps);
            this.Controls.Add(this.lblFps);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "StrSettings";
            this.Load += new System.EventHandler(this.FrmSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numFps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScreenWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScreenHeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFps;
        private System.Windows.Forms.NumericUpDown numFps;
        private System.Windows.Forms.LinkLabel lnkDefaultFps;
        private System.Windows.Forms.Label lblScreenResolution;
        private System.Windows.Forms.NumericUpDown numScreenWidth;
        private System.Windows.Forms.NumericUpDown numScreenHeight;
        private System.Windows.Forms.LinkLabel lnkDefaultResolution;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAccept;
    }
}
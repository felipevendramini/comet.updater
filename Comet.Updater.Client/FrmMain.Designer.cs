﻿namespace Comet.Updater.Client
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnPlayHigh = new System.Windows.Forms.Button();
            this.btnPlayLow = new System.Windows.Forms.Button();
            this.panelDownloadProgress = new System.Windows.Forms.Panel();
            this.splitDownloadContainer = new System.Windows.Forms.SplitContainer();
            this.pbCurrentProgress = new System.Windows.Forms.ProgressBar();
            this.lblDownloadProgress = new System.Windows.Forms.Label();
            this.btnRegister = new System.Windows.Forms.Button();
            this.btnDownloads = new System.Windows.Forms.Button();
            this.btnRanking = new System.Windows.Forms.Button();
            this.btnSite = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.panelStatus = new System.Windows.Forms.Panel();
            this.lnkTos = new System.Windows.Forms.LinkLabel();
            this.lnkSettings = new System.Windows.Forms.LinkLabel();
            this.lnkPrivacy = new System.Windows.Forms.LinkLabel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.panelDownloadProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitDownloadContainer)).BeginInit();
            this.splitDownloadContainer.Panel1.SuspendLayout();
            this.splitDownloadContainer.Panel2.SuspendLayout();
            this.splitDownloadContainer.SuspendLayout();
            this.panelStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPlayHigh
            // 
            this.btnPlayHigh.BackColor = System.Drawing.Color.Transparent;
            this.btnPlayHigh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlayHigh.FlatAppearance.BorderSize = 0;
            this.btnPlayHigh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPlayHigh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnPlayHigh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayHigh.Image = ((System.Drawing.Image)(resources.GetObject("btnPlayHigh.Image")));
            this.btnPlayHigh.Location = new System.Drawing.Point(596, 426);
            this.btnPlayHigh.Name = "btnPlayHigh";
            this.btnPlayHigh.Size = new System.Drawing.Size(210, 83);
            this.btnPlayHigh.TabIndex = 0;
            this.btnPlayHigh.UseVisualStyleBackColor = false;
            this.btnPlayHigh.Click += new System.EventHandler(this.btnPlayHigh_Click);
            this.btnPlayHigh.MouseEnter += new System.EventHandler(this.btnPlayHigh_MouseEnter);
            this.btnPlayHigh.MouseLeave += new System.EventHandler(this.btnPlayHigh_MouseLeave);
            // 
            // btnPlayLow
            // 
            this.btnPlayLow.BackColor = System.Drawing.Color.Transparent;
            this.btnPlayLow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlayLow.FlatAppearance.BorderSize = 0;
            this.btnPlayLow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPlayLow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnPlayLow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlayLow.Image = ((System.Drawing.Image)(resources.GetObject("btnPlayLow.Image")));
            this.btnPlayLow.Location = new System.Drawing.Point(588, 498);
            this.btnPlayLow.Name = "btnPlayLow";
            this.btnPlayLow.Size = new System.Drawing.Size(210, 83);
            this.btnPlayLow.TabIndex = 0;
            this.btnPlayLow.UseVisualStyleBackColor = false;
            this.btnPlayLow.Click += new System.EventHandler(this.btnPlayLow_Click);
            this.btnPlayLow.MouseEnter += new System.EventHandler(this.btnPlayLow_MouseEnter);
            this.btnPlayLow.MouseLeave += new System.EventHandler(this.btnPlayLow_MouseLeave);
            // 
            // panelDownloadProgress
            // 
            this.panelDownloadProgress.BackColor = System.Drawing.Color.Transparent;
            this.panelDownloadProgress.Controls.Add(this.splitDownloadContainer);
            this.panelDownloadProgress.Location = new System.Drawing.Point(36, 450);
            this.panelDownloadProgress.Name = "panelDownloadProgress";
            this.panelDownloadProgress.Size = new System.Drawing.Size(552, 72);
            this.panelDownloadProgress.TabIndex = 1;
            // 
            // splitDownloadContainer
            // 
            this.splitDownloadContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitDownloadContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitDownloadContainer.Location = new System.Drawing.Point(0, 0);
            this.splitDownloadContainer.Name = "splitDownloadContainer";
            this.splitDownloadContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitDownloadContainer.Panel1
            // 
            this.splitDownloadContainer.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.splitDownloadContainer.Panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("splitDownloadContainer.Panel1.BackgroundImage")));
            this.splitDownloadContainer.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.splitDownloadContainer.Panel1.Controls.Add(this.pbCurrentProgress);
            // 
            // splitDownloadContainer.Panel2
            // 
            this.splitDownloadContainer.Panel2.Controls.Add(this.lblDownloadProgress);
            this.splitDownloadContainer.Size = new System.Drawing.Size(552, 72);
            this.splitDownloadContainer.SplitterDistance = 43;
            this.splitDownloadContainer.TabIndex = 0;
            // 
            // pbCurrentProgress
            // 
            this.pbCurrentProgress.Location = new System.Drawing.Point(17, 5);
            this.pbCurrentProgress.Name = "pbCurrentProgress";
            this.pbCurrentProgress.Size = new System.Drawing.Size(515, 30);
            this.pbCurrentProgress.Step = 1;
            this.pbCurrentProgress.TabIndex = 0;
            // 
            // lblDownloadProgress
            // 
            this.lblDownloadProgress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDownloadProgress.ForeColor = System.Drawing.Color.White;
            this.lblDownloadProgress.Location = new System.Drawing.Point(0, 0);
            this.lblDownloadProgress.Name = "lblDownloadProgress";
            this.lblDownloadProgress.Size = new System.Drawing.Size(552, 25);
            this.lblDownloadProgress.TabIndex = 0;
            this.lblDownloadProgress.Text = "StrLoading";
            this.lblDownloadProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRegister
            // 
            this.btnRegister.BackColor = System.Drawing.Color.Transparent;
            this.btnRegister.FlatAppearance.BorderSize = 0;
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegister.Image = ((System.Drawing.Image)(resources.GetObject("btnRegister.Image")));
            this.btnRegister.Location = new System.Drawing.Point(32, 528);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(108, 30);
            this.btnRegister.TabIndex = 2;
            this.btnRegister.UseVisualStyleBackColor = false;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            this.btnRegister.MouseEnter += new System.EventHandler(this.btnRegister_MouseEnter);
            this.btnRegister.MouseLeave += new System.EventHandler(this.btnRegister_MouseLeave);
            // 
            // btnDownloads
            // 
            this.btnDownloads.BackColor = System.Drawing.Color.Transparent;
            this.btnDownloads.FlatAppearance.BorderSize = 0;
            this.btnDownloads.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownloads.Image = ((System.Drawing.Image)(resources.GetObject("btnDownloads.Image")));
            this.btnDownloads.Location = new System.Drawing.Point(146, 528);
            this.btnDownloads.Name = "btnDownloads";
            this.btnDownloads.Size = new System.Drawing.Size(108, 30);
            this.btnDownloads.TabIndex = 2;
            this.btnDownloads.UseVisualStyleBackColor = false;
            this.btnDownloads.Click += new System.EventHandler(this.btnDownloads_Click);
            this.btnDownloads.MouseEnter += new System.EventHandler(this.btnDownloads_MouseEnter);
            this.btnDownloads.MouseLeave += new System.EventHandler(this.btnDownloads_MouseLeave);
            // 
            // btnRanking
            // 
            this.btnRanking.BackColor = System.Drawing.Color.Transparent;
            this.btnRanking.FlatAppearance.BorderSize = 0;
            this.btnRanking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRanking.Image = ((System.Drawing.Image)(resources.GetObject("btnRanking.Image")));
            this.btnRanking.Location = new System.Drawing.Point(260, 528);
            this.btnRanking.Name = "btnRanking";
            this.btnRanking.Size = new System.Drawing.Size(108, 30);
            this.btnRanking.TabIndex = 2;
            this.btnRanking.UseVisualStyleBackColor = false;
            this.btnRanking.Click += new System.EventHandler(this.btnRanking_Click);
            this.btnRanking.MouseEnter += new System.EventHandler(this.btnRanking_MouseEnter);
            this.btnRanking.MouseLeave += new System.EventHandler(this.btnRanking_MouseLeave);
            // 
            // btnSite
            // 
            this.btnSite.BackColor = System.Drawing.Color.Transparent;
            this.btnSite.FlatAppearance.BorderSize = 0;
            this.btnSite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSite.Image = ((System.Drawing.Image)(resources.GetObject("btnSite.Image")));
            this.btnSite.Location = new System.Drawing.Point(374, 528);
            this.btnSite.Name = "btnSite";
            this.btnSite.Size = new System.Drawing.Size(108, 30);
            this.btnSite.TabIndex = 2;
            this.btnSite.UseVisualStyleBackColor = false;
            this.btnSite.Click += new System.EventHandler(this.btnSite_Click);
            this.btnSite.MouseEnter += new System.EventHandler(this.btnSite_MouseEnter);
            this.btnSite.MouseLeave += new System.EventHandler(this.btnSite_MouseLeave);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.Location = new System.Drawing.Point(488, 528);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(108, 30);
            this.btnExit.TabIndex = 2;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseEnter += new System.EventHandler(this.btnExit_MouseEnter);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnExit_MouseLeave);
            // 
            // lblStatus
            // 
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.ForeColor = System.Drawing.Color.White;
            this.lblStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(552, 72);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "StrLoading";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelStatus
            // 
            this.panelStatus.BackColor = System.Drawing.Color.Transparent;
            this.panelStatus.Controls.Add(this.lblStatus);
            this.panelStatus.Location = new System.Drawing.Point(36, 450);
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(552, 72);
            this.panelStatus.TabIndex = 3;
            // 
            // lnkTos
            // 
            this.lnkTos.AutoSize = true;
            this.lnkTos.BackColor = System.Drawing.Color.Transparent;
            this.lnkTos.LinkColor = System.Drawing.Color.Yellow;
            this.lnkTos.Location = new System.Drawing.Point(89, 561);
            this.lnkTos.Name = "lnkTos";
            this.lnkTos.Size = new System.Drawing.Size(60, 15);
            this.lnkTos.TabIndex = 4;
            this.lnkTos.TabStop = true;
            this.lnkTos.Text = "linkLabel1";
            this.lnkTos.VisitedLinkColor = System.Drawing.Color.Yellow;
            // 
            // lnkSettings
            // 
            this.lnkSettings.AutoSize = true;
            this.lnkSettings.BackColor = System.Drawing.Color.Transparent;
            this.lnkSettings.LinkColor = System.Drawing.Color.Yellow;
            this.lnkSettings.Location = new System.Drawing.Point(496, 561);
            this.lnkSettings.Name = "lnkSettings";
            this.lnkSettings.Size = new System.Drawing.Size(60, 15);
            this.lnkSettings.TabIndex = 4;
            this.lnkSettings.TabStop = true;
            this.lnkSettings.Text = "linkLabel1";
            this.lnkSettings.VisitedLinkColor = System.Drawing.Color.Yellow;
            this.lnkSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkSettings_LinkClicked);
            // 
            // lnkPrivacy
            // 
            this.lnkPrivacy.AutoSize = true;
            this.lnkPrivacy.BackColor = System.Drawing.Color.Transparent;
            this.lnkPrivacy.LinkColor = System.Drawing.Color.Yellow;
            this.lnkPrivacy.Location = new System.Drawing.Point(277, 561);
            this.lnkPrivacy.Name = "lnkPrivacy";
            this.lnkPrivacy.Size = new System.Drawing.Size(60, 15);
            this.lnkPrivacy.TabIndex = 4;
            this.lnkPrivacy.TabStop = true;
            this.lnkPrivacy.Text = "linkLabel1";
            this.lnkPrivacy.VisitedLinkColor = System.Drawing.Color.Yellow;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.ForeColor = System.Drawing.Color.White;
            this.lblVersion.Location = new System.Drawing.Point(53, 37);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(59, 15);
            this.lblVersion.TabIndex = 5;
            this.lblVersion.Text = "StrVersion";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(804, 600);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lnkPrivacy);
            this.Controls.Add(this.lnkSettings);
            this.Controls.Add(this.lnkTos);
            this.Controls.Add(this.panelStatus);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSite);
            this.Controls.Add(this.btnRanking);
            this.Controls.Add(this.btnDownloads);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.panelDownloadProgress);
            this.Controls.Add(this.btnPlayLow);
            this.Controls.Add(this.btnPlayHigh);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "World Conquer Online - Launcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.LocationChanged += new System.EventHandler(this.FrmMain_LocationChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseMove);
            this.panelDownloadProgress.ResumeLayout(false);
            this.splitDownloadContainer.Panel1.ResumeLayout(false);
            this.splitDownloadContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitDownloadContainer)).EndInit();
            this.splitDownloadContainer.ResumeLayout(false);
            this.panelStatus.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPlayHigh;
        private System.Windows.Forms.Button btnPlayLow;
        private System.Windows.Forms.Panel panelDownloadProgress;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.Button btnDownloads;
        private System.Windows.Forms.Button btnRanking;
        private System.Windows.Forms.Button btnSite;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.SplitContainer splitDownloadContainer;
        private System.Windows.Forms.Label lblDownloadProgress;
        internal System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Panel panelStatus;
        private System.Windows.Forms.LinkLabel lnkTos;
        private System.Windows.Forms.LinkLabel lnkSettings;
        private System.Windows.Forms.LinkLabel lnkPrivacy;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.ProgressBar pbCurrentProgress;
    }
}


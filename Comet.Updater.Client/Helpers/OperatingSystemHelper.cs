﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - OperatingSystemHelper.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/24 15:24
// ===================================================================

#endregion

#region References

using System;
using System.Management;

#endregion

namespace Comet.Updater.Client.Helpers
{
    public static class OperatingSystemHelper
    {
        private static readonly OperatingSystem mOS = Environment.OSVersion;
        private static readonly Version mWinVersion = mOS.Version;

        public static string OperatingSystemName
        {
            get
            {
                //Variable to hold our return value
                string operatingSystem = "";

                if (mOS.Platform == PlatformID.Win32Windows)
                    //This is a pre-NT version of Windows
                    switch (mWinVersion.Minor)
                    {
                        case 0:
                            operatingSystem = "95";
                            break;
                        case 10:
                            if (mWinVersion.Revision.ToString() == "2222A")
                                operatingSystem = "98SE";
                            else
                                operatingSystem = "98";
                            break;
                        case 90:
                            operatingSystem = "Me";
                            break;
                    }
                else if (mOS.Platform == PlatformID.Win32NT)
                    switch (mWinVersion.Major)
                    {
                        case 3:
                            operatingSystem = "NT 3.51";
                            break;
                        case 4:
                            operatingSystem = "NT 4.0";
                            break;
                        case 5:
                            if (mWinVersion.Minor == 0)
                                operatingSystem = "2000";
                            else
                                operatingSystem = "XP";
                            break;
                        case 6:
                            if (mWinVersion.Minor == 0)
                                operatingSystem = "Vista";
                            else if (mWinVersion.Minor == 1)
                                operatingSystem = "7";
                            else if (mWinVersion.Minor == 2)
                                operatingSystem = "8";
                            else
                                operatingSystem = "8.1";
                            break;
                        case 10:
                            operatingSystem = "10";
                            break;
                    }

                //Make sure we actually got something in our OS check
                //We don't want to just return " Service Pack 2" or " 32-bit"
                //That information is useless without the OS version.
                if (!string.IsNullOrEmpty(operatingSystem))
                {
                    //Got something.  Let's prepend "Windows" and get more info.
                    operatingSystem = "Windows " + operatingSystem;
                    //See if there's a service pack installed.
                    if (mOS.ServicePack != "")
                        //Append it to the OS name.  i.e. "Windows XP Service Pack 3"
                        operatingSystem += " " + mOS.ServicePack;
                    //Append the OS architecture.  i.e. "Windows XP Service Pack 3 32-bit"
                    operatingSystem += " " + (Environment.Is64BitOperatingSystem ? "64" : "32") + "-bit";
                }

                //Return the information we've gathered.
                return operatingSystem;
            }
        }

        public static bool IsWindows7() => mWinVersion.Major == 6 && mWinVersion.Minor == 1;

        public static bool IsWindows7OrHigher() => OperatingSystem.IsWindowsVersionAtLeast(6, 1);

        public static bool HasAntivirus()
        {
            if (!IsWindows7OrHigher())
                return false;

            string scope = @"root\SecurityCenter2";
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, "SELECT * FROM AntivirusProduct");
                ManagementObjectCollection instances = searcher.Get();
                return instances.Count > 0;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return false;
        }
    }
}
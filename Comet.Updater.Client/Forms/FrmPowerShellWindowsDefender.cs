﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - FrmPowerShellWindowsDefender.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/24 12:01
// ===================================================================

#endregion

#region References

using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Comet.Updater.Client.Configuration;
using Comet.Updater.Client.Helpers;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Client.Forms
{
    public partial class FrmPowerShellWindowsDefender : Form
    {
        public FrmPowerShellWindowsDefender()
        {
            InitializeComponent();

            SuspendLayout();

            btnAccept.FlatAppearance.MouseOverBackColor = Color.Transparent;
            btnAccept.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnCancel.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnCancel.FlatAppearance.MouseOverBackColor = Color.Transparent;

            ResumeLayout();
        }

        private async void btnAccept_Click(object sender, EventArgs e)
        {
            if (!OperatingSystemHelper.IsWindows7OrHigher())
            {
                MessageBox.Show(this, LanguageManager.GetString("StrInvalidOS"));
                Close();
                return;
            }

            try
            {
                Process process = new Process
                {
                    StartInfo =
                    {
                        UseShellExecute = false,
                        FileName = "cmd",
                        Arguments = $"/c powershell -inputformat none -outputformat none -NonInteractive -Command Add-MpPreference -ExclusionPath '{Environment.CurrentDirectory}'",
                        RedirectStandardOutput = true,
                        WorkingDirectory = Environment.CurrentDirectory,
                        CreateNoWindow = true,
                        WindowStyle = ProcessWindowStyle.Hidden
                    }
                };
                process.Start();
                string output = await process.StandardOutput.ReadToEndAsync();

                await process.WaitForExitAsync();

                if (!string.IsNullOrEmpty(output))
                    await Log.WriteLogAsync(LogLevel.Message, $"FrmPowerShellWindowsDefender Accept Result: {output}");
            }
            catch (Exception exception)
            {
                MessageBox.Show(this, LanguageManager.GetString("StrAccessDeniedEx", exception.Message));
                Close();
                return;
            }

            UserConfigurationManager.Configuration.PowerShellWindowsDefender = DateTime.Now;
            UserConfigurationManager.Configuration.Save();

            Close();
        }

        private void FrmPowerShellWindowsDefender_Load(object sender, EventArgs e)
        {
            lblMessage.Text = LanguageManager.GetString(lblMessage.Text);
            btnAccept.Text = LanguageManager.GetString(btnAccept.Text);
            btnCancel.Text = LanguageManager.GetString(btnCancel.Text);
            lnkReadMore.Text = LanguageManager.GetString(lnkReadMore.Text);
        }

        private void lnkReadMore_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show("TODO");
            //Kernel.OpenUrl("");
        }
    }
}
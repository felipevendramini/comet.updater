﻿
namespace Comet.Updater.Client.Forms
{
    partial class FrmPowerShellWindowsDefender
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lnkReadMore = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblMessage.Location = new System.Drawing.Point(31, 28);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(335, 128);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.Text = "StrWindowsDefenderAlert";
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.Transparent;
            this.btnAccept.BackgroundImage = global::Comet.Updater.Client.Properties.Resources.More;
            this.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAccept.FlatAppearance.BorderSize = 0;
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccept.Font = new System.Drawing.Font("SimSun", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAccept.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAccept.Location = new System.Drawing.Point(186, 159);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(94, 32);
            this.btnAccept.TabIndex = 7;
            this.btnAccept.Text = "StrYes";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BackgroundImage = global::Comet.Updater.Client.Properties.Resources.More;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("SimSun", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCancel.Location = new System.Drawing.Point(284, 159);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(94, 32);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "StrNo";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // lnkReadMore
            // 
            this.lnkReadMore.AutoSize = true;
            this.lnkReadMore.BackColor = System.Drawing.Color.Transparent;
            this.lnkReadMore.Location = new System.Drawing.Point(31, 168);
            this.lnkReadMore.Name = "lnkReadMore";
            this.lnkReadMore.Size = new System.Drawing.Size(75, 15);
            this.lnkReadMore.TabIndex = 9;
            this.lnkReadMore.TabStop = true;
            this.lnkReadMore.Text = "StrReadMore";
            this.lnkReadMore.VisitedLinkColor = System.Drawing.Color.Blue;
            this.lnkReadMore.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkReadMore_LinkClicked);
            // 
            // FrmPowerShellWindowsDefender
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Comet.Updater.Client.Properties.Resources.Hint;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(399, 207);
            this.Controls.Add(this.lnkReadMore);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblMessage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPowerShellWindowsDefender";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StrWindowsDefenderAlertTitle";
            this.Load += new System.EventHandler(this.FrmPowerShellWindowsDefender_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.LinkLabel lnkReadMore;
    }
}
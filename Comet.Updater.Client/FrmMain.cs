﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - FrmMain.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/02/02 16:30
// ===================================================================

#endregion

#region References

using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using Comet.Updater.Client.Communication.Packets;
using Comet.Updater.Client.Configuration;
using Comet.Updater.Client.Forms;
using Comet.Updater.Client.Helpers;
using Comet.Updater.Client.Properties;
using Comet.Updater.Client.Threads;
using Comet.Updater.Files.Web;
using Comet.Updater.Network.Packets.Structures;

#endregion

namespace Comet.Updater.Client
{
    public partial class FrmMain : Form
    {
        public const int CURRENT_VERSION = 10011;
        public static int CurrentGameVersion = 1000;

        private BaseThread mThread;
        private bool mInternalCloseRequest;
        private List<Process> mGameProcesses = new List<Process>();

        public FrmMain()
        {
            InitializeComponent();

            SuspendLayout();

            // since we need transparent BG to images, we set it at startup
            AllowTransparency = true;
            TransparencyKey = Color.FromArgb(0xff00d8);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            BackColor = Color.FromArgb(0x00FFFFFF);
            DoubleBuffered = true;

            btnPlayHigh.FlatAppearance.MouseOverBackColor = Color.Transparent;
            btnPlayHigh.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnPlayLow.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnPlayLow.FlatAppearance.MouseOverBackColor = Color.Transparent;

            btnRegister.FlatAppearance.MouseOverBackColor = Color.Transparent;
            btnRegister.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnDownloads.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnDownloads.FlatAppearance.MouseOverBackColor = Color.Transparent;
            btnRanking.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnRanking.FlatAppearance.MouseOverBackColor = Color.Transparent;
            btnSite.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnSite.FlatAppearance.MouseOverBackColor = Color.Transparent;
            btnExit.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnExit.FlatAppearance.MouseOverBackColor = Color.Transparent;

            LanguageManager.SetLanguage(CultureInfo.CurrentUICulture.Name);

            ResumeLayout();

            Kernel.Configuration = new ConfigurationManager();
        }

        #region Form Events

        #region Initialization

        private async void FrmMain_Load(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            lblDownloadProgress.Text = "";

            lnkTos.Text = LanguageManager.GetString("StrTermsOfService");
            lnkPrivacy.Text = LanguageManager.GetString("StrTermsOfPrivacy");
            lnkSettings.Text = LanguageManager.GetString("StrSettings");

            CheckVersionFile();

            lblVersion.Text = LanguageManager.GetString("StrVersion", CurrentGameVersion, CURRENT_VERSION);

            UserConfigurationManager.Configuration = new UserConfigurationManager();
            if (!UserConfigurationManager.Configuration.Initialize())
            {
                if (OpenDialog(typeof(FrmSettings)) != DialogResult.OK)
                {
                    MessageBox.Show(this, LanguageManager.GetString("StrSettingsNotLoaded"));
                }
                else if (!UserConfigurationManager.Configuration.Initialize())
                {
                    MessageBox.Show(this, LanguageManager.GetString("StrCouldNotLoadConfigFile"));
                    return;
                }
            }

            if (OperatingSystemHelper.IsWindows7OrHigher() && OperatingSystemHelper.HasAntivirus())
            {
                if (UserConfigurationManager.Configuration.PowerShellWindowsDefender == null)
                {
                    new FrmPowerShellWindowsDefender().ShowDialog(this);
                }
            }

            SetGameReady(false);
            SetProgressStatus(false);

            mThread = new BaseThread();
            await mThread.StartAsync();
        }

        private async void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!mInternalCloseRequest)
            {
                if (mGameProcesses.Count > 0
                    && MessageBox.Show(this, LanguageManager.GetString("StrCloseMessageClientOpen"),
                        LanguageManager.GetString("StrCloseTitle"), MessageBoxButtons.YesNo) != DialogResult.Yes)
                {
                    e.Cancel = true;
                    return;
                }
            }
            else
            {
                
            }

            SetGameReady(false);

            if (mThread != null)
                await mThread.CloseAsync();
        }

        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var game in mGameProcesses)
            {
                try
                {
                    game.Kill(true);
                    game.Close();
                    game.Dispose();
                }
                catch
                {
                    // nothing
                }
            }

            if (!string.IsNullOrEmpty(mClientUpdatePath))
            {
                Process.Start(mClientUpdatePath, $"-d \"{Environment.CurrentDirectory}\"");
            }
            else
            {
                Kernel.OpenUrl("https://worldconquer.online/News");
            }
        }

        #endregion

        #region Drag and Drop

        private Point m_mouseLoc;

        private void FrmMain_MouseDown(object sender, MouseEventArgs e)
        {
            m_mouseLoc = e.Location;
        }

        private void FrmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int dx = e.Location.X - m_mouseLoc.X;
                int dy = e.Location.Y - m_mouseLoc.Y;
                Location = new Point(Location.X + dx, Location.Y + dy);
            }
        }

        private void FrmMain_LocationChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        #endregion

        #region Buttons

        #region Hover

        private void btnRegister_MouseEnter(object sender, EventArgs e)
        {
            btnRegister.Image = Resources.Register02;
        }

        private void btnRegister_MouseLeave(object sender, EventArgs e)
        {
            btnRegister.Image = Resources.Register01;
        }

        private void btnDownloads_MouseEnter(object sender, EventArgs e)
        {
            btnDownloads.Image = Resources.Download02;
        }

        private void btnDownloads_MouseLeave(object sender, EventArgs e)
        {
            btnDownloads.Image = Resources.Download;
        }

        private void btnRanking_MouseEnter(object sender, EventArgs e)
        {
            btnRanking.Image = Resources.Ranking02;
        }

        private void btnRanking_MouseLeave(object sender, EventArgs e)
        {
            btnRanking.Image = Resources.Ranking;
        }

        private void btnSite_MouseEnter(object sender, EventArgs e)
        {
            btnSite.Image = Resources.Office02;
        }

        private void btnSite_MouseLeave(object sender, EventArgs e)
        {
            btnSite.Image = Resources.Office01;
        }

        private void btnExit_MouseEnter(object sender, EventArgs e)
        {
            btnExit.Image = Resources.Exit02;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.Image = Resources.Exit01;
        }

        private void btnPlayHigh_MouseEnter(object sender, EventArgs e)
        {
            btnPlayHigh.Image = Resources.high021;
        }

        private void btnPlayHigh_MouseLeave(object sender, EventArgs e)
        {
            btnPlayHigh.Image = Resources.high031;
        }

        private void btnPlayLow_MouseEnter(object sender, EventArgs e)
        {
            btnPlayLow.Image = Resources.low021;
        }

        private void btnPlayLow_MouseLeave(object sender, EventArgs e)
        {
            btnPlayLow.Image = Resources.low031;
        }

        #endregion

        #region Click events

        private void btnPlayHigh_Click(object sender, EventArgs e)
        {
        }

        private void btnPlayLow_Click(object sender, EventArgs e)
        {
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            Kernel.OpenUrl("https://worldconquer.online/Register");
        }

        private void btnDownloads_Click(object sender, EventArgs e)
        {
            Kernel.OpenUrl("https://worldconquer.online/Downloads");
        }

        private void btnRanking_Click(object sender, EventArgs e)
        {
            Kernel.OpenUrl("https://worldconquer.online/Ranking/Players/All");
        }

        private void btnSite_Click(object sender, EventArgs e)
        {
            Kernel.OpenUrl("https://worldconquer.online");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Kernel.OpenUrl("https://worldconquer.online/News");
            Close();
        }

        #endregion

        #endregion

        #region Screen Refresh

        #endregion

        #region Form Update

        public DialogResult OpenDialog(Type t)
        {
            if (typeof(FrmSettings) == t)
                return new FrmSettings().ShowDialog(this);

            return DialogResult.Abort;
        }

        public void SetStatusLabel(string text)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(SetStatusLabel), text);
                return;
            }

            if (panelDownloadProgress.Visible)
            {
                lblDownloadProgress.Text = text;
            }
            else
            {
                lblStatus.Text = text;
            }
        }

        public void SetGameReady(bool enable)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<bool>(SetGameReady), enable);
                return;
            }

            btnPlayHigh.Enabled = enable;
            btnPlayLow.Enabled = enable;
            btnExit.Enabled = enable;
        }

        public void SetProgressStatus(bool download)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<bool>(SetProgressStatus), download);
                return;
            }

            panelDownloadProgress.Visible = download;
            panelStatus.Visible = !download;
        }

        public void SetProgressBarValue(int value)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<int>(SetProgressBarValue), value);
                return;
            }

            pbCurrentProgress.Value = Math.Min(pbCurrentProgress.Maximum, Math.Max(value, pbCurrentProgress.Minimum));
        }

        public void SetProgressBarMinValue(int value)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<int>(SetProgressBarMinValue), value);
                return;
            }
            pbCurrentProgress.Minimum = value;
        }

        public void SetProgressBarMaxValue(int value)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<int>(SetProgressBarMaxValue), value);
                return;
            }
            pbCurrentProgress.Maximum = value;
        }

        #endregion

        #region Link Events

        private void lnkSettings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (OpenDialog(typeof(FrmSettings)) == DialogResult.OK)
            {
                UserConfigurationManager.Configuration = new UserConfigurationManager();
                UserConfigurationManager.Configuration.Initialize();
            }
        }

        #endregion

        #endregion

        #region Download Files

        private WebClient mWebClient;
        private Stopwatch mDownloadStopwatch;
        private Queue<string> mDownloadsQueue = new Queue<string>(); // files to download
        private Queue<string> mDownloadedQueue = new Queue<string>(); // downloaded files
        private string mClientUpdatePath = string.Empty;
        private string mCurrentFileName;
        private int mTotalFileSize = 0;
        private int mTotalDownloaded = 0;
        private int mTotalFilesDownload = 0;
        private long mCurrentlyDownloadedBytes = 0;
        private int mLastDownloadTick = 0;

        public string DownloadPath
        {
            get
            {
                string path = Path.Combine(Environment.CurrentDirectory, "AutoPatch");
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                new DirectoryInfo(path).Attributes &= ~FileAttributes.ReadOnly;
                path = Path.Combine(path, "temp");
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                new DirectoryInfo(path).Attributes &= ~FileAttributes.ReadOnly;
                return path;
            }
        }

        public void NoDownload()
        {
            SetProgressStatus(false);
            SetStatusLabel(LanguageManager.GetString("StrReadyToLogin"));
            SetGameReady(true);
        }

        public async Task ProcessDownloadAsync(string url, int latestVersion, List<MsgAutoPatcherPatchInfo<Communication.Client>.UpdateInfo> infos)
        {
            // we don't want to enqueue stuff until we finish what we're doing
            if (mDownloadsQueue.Count > 0)
                return;

            int totalFilesSize = 0;
            int fileNum = 0;
            url = url.Trim('/');

            foreach (var info in infos.OrderBy(x => x.To))
            {
                string downloadUrl = $"{url}/{info.File}";
                long fileSize = await RemoteFilesHelper.FetchFileSizeAsync(downloadUrl);
                if (fileSize < 0)
                {
                    SetStatusLabel(LanguageManager.GetString("StrFailedToFetchFile", info.File));
                    SetGameReady(false);
                    return;
                }
                totalFilesSize += (int) fileSize;
                fileNum += 1;
                mDownloadsQueue.Enqueue(downloadUrl);
                SetStatusLabel(LanguageManager.GetString("StrLabelCalculatingDownloadAmount", fileNum, RemoteFilesHelper.ParseFileSize(totalFilesSize)));
            }

            mTotalFileSize = totalFilesSize;
            mTotalFilesDownload = infos.Count;

            SetStatusLabel(LanguageManager.GetString("StrLabelCalculatingDownloadAmount", mTotalFilesDownload, RemoteFilesHelper.ParseFileSize(mTotalFileSize)));

            SetProgressBarValue(0);
            SetProgressBarMinValue(0);
            SetProgressBarMaxValue(mTotalFileSize);

            SetProgressStatus(true);
            await BeginDownloadAsync();
        }

        private async Task BeginDownloadAsync()
        {
            if (mDownloadsQueue.Count == 0 || !string.IsNullOrEmpty(mClientUpdatePath))
            {
                SetProgressBarValue(pbCurrentProgress.Maximum);

                if (mDownloadedQueue.Count > 0)
                {
                    await BeginInstallAsync();
                }
                else
                {
                    if (string.IsNullOrEmpty(mClientUpdatePath) && Kernel.Client != null)
                    {
                        await Kernel.Client.SendAsync(new MsgAutoPatcherCheckInfo
                        {
                            Version = CurrentGameVersion,
                            Action = MsgAutoPatcherCheckInfo<Communication.Client>.PacketAction.ClientVersion
                        });
                    }
                }
                return;
            }

            string currentFile = mDownloadsQueue.Dequeue();
            string fileName = mCurrentFileName = Path.GetFileName(currentFile);

            mTotalDownloaded++;

            mDownloadedQueue.Enqueue(currentFile);

            mDownloadStopwatch = Stopwatch.StartNew();

            mCurrentlyDownloadedBytes = 0;
            mLastDownloadTick = 0;

            mWebClient = new WebClient();
            mWebClient.DownloadProgressChanged += FileDownloaderProgressChanged;
            mWebClient.DownloadFileCompleted += FileDownloaderDownloadCompleted;
            await mWebClient.DownloadFileTaskAsync(new Uri(currentFile), Path.Combine(DownloadPath, fileName));
        }

        private void FileDownloaderDownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            mWebClient.Dispose();
            mDownloadStopwatch.Stop();

            BeginDownloadAsync().ConfigureAwait(false);
        }

        private void FileDownloaderProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            SetProgressBarValue((int) (pbCurrentProgress.Value + (e.BytesReceived - mCurrentlyDownloadedBytes)));
            mCurrentlyDownloadedBytes = e.BytesReceived;

            if (Environment.TickCount - mLastDownloadTick > 1000)
            {
                SetStatusLabel(LanguageManager.GetString("StrLabelDownloading",
                    mCurrentFileName,
                    mTotalFilesDownload,
                    RemoteFilesHelper.ParseFileSize(pbCurrentProgress.Value),
                    RemoteFilesHelper.ParseFileSize(mTotalFileSize),
                    RemoteFilesHelper.ParseDownloadSpeed((long)(e.BytesReceived / mDownloadStopwatch.Elapsed.TotalSeconds))));
                mLastDownloadTick = Environment.TickCount;
            }
        }

        #endregion

        #region Extract and Install

        private async Task BeginInstallAsync()
        {
            SetProgressStatus(false);
            SetStatusLabel(LanguageManager.GetString("StrInstallUpdates"));

            while (mDownloadedQueue.TryDequeue(out string result))
            {
                string tempName = Path.GetFileNameWithoutExtension(result);
                string extension = Path.GetExtension(result);
                string installPath = Path.Combine(DownloadPath, Path.GetFileName(result));

                if (int.TryParse(tempName, out var patchNum) && patchNum >= 10000)
                {
                    mClientUpdatePath = installPath;
                    mInternalCloseRequest = true;
                    Close();
                    return;
                }

                SetStatusLabel(LanguageManager.GetString("StrInstallUpdate", tempName));

                if (extension.Equals(".exe", StringComparison.InvariantCulture))
                {
                    try
                    {
                        Process process = Process.Start(installPath, $"-s -d \"{Environment.CurrentDirectory}\"");
                        if (process == null)
                        {
                            MessageBox.Show(this, LanguageManager.GetString("StrInstallCouldNotStartProcess"));
                            return;
                        }

                        await process.WaitForExitAsync();
                        process.Close();
                        process.Dispose();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(this, LanguageManager.GetString("StrInstallIOException", e.Message));
                        return;
                    }
                    finally
                    {
                        CheckVersionFile();
                    }
                }
                else
                {
                    MessageBox.Show(this, LanguageManager.GetString("StrInstallInvalidExt", extension));
                    continue;
                }
            }

            SetStatusLabel(LanguageManager.GetString("StrInstallSuccess"));

            CheckVersionFile();

            Kernel.ClientStage = Kernel.Stage.WaitingForClientUpdates;
            await Kernel.Client.SendAsync(new MsgAutoPatcherCheckInfo
            {
                Version = CurrentGameVersion,
                Action = MsgAutoPatcherCheckInfo<Communication.Client>.PacketAction.ClientVersion
            });
        }

        #endregion

        #region Versioning

        private void CheckVersionFile()
        {
            if (!File.Exists("./Version.dat")) MessageBox.Show(this, LanguageManager.GetString("StrVersionNotFound"));

            string version = File.ReadAllText("./Version.dat");
            if (string.IsNullOrEmpty(version)) MessageBox.Show(this, LanguageManager.GetString("StrVersionNotFound"));

            CurrentGameVersion = int.Parse(version);
        }

        #endregion
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - LanguageManager.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/02/05 12:00
// ===================================================================

#endregion

#region References

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading;

#endregion

namespace Comet.Updater
{
    public sealed class LanguageManager
    {
        public static List<Languages> AvailableLanguages = new List<Languages>
        {
            new Languages {LanguageFullName = "English", LanguageCultureName = "en-US"},
            new Languages {LanguageFullName = "Português Brasileiro", LanguageCultureName = "pt-BR"}
        };

        public static string CurrentlySelectedLanguage;

        public static bool IsLanguageAvailable(string lang)
        {
            return AvailableLanguages.FirstOrDefault(a => a.LanguageCultureName.Equals(lang)) != null;
        }

        public static string GetDefaultLanguage()
        {
            return AvailableLanguages[0].LanguageCultureName;
        }

        public static void SetLanguage(string lang)
        {
            try
            {
                if (!IsLanguageAvailable(lang)) lang = GetDefaultLanguage();
                var cultureInfo = new CultureInfo(lang);
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);

                CurrentlySelectedLanguage = cultureInfo.IetfLanguageTag;
                LanguageResource.Initialize();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static string GetString(string name, params object[] strs)
        {
            string result = LanguageResource.ResourceManager.GetString(name);
            return !string.IsNullOrEmpty(result) ? string.Format(result, strs) : name;
        }
    }

    public class LanguageResource
    {
        public static ResourceManager ResourceManager;

        public static void Initialize()
        {
            ResourceManager = new ResourceManager(
                $"Comet.Updater.Client.Localization.Language_{LanguageManager.CurrentlySelectedLanguage}",
                Assembly.GetExecutingAssembly());
        }
    }

    public class Languages
    {
        public string LanguageFullName { get; set; }
        public string LanguageCultureName { get; set; }
    }
}
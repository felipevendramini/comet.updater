#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - Program.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/02/02 16:30
// ===================================================================

#endregion

#region References

using System;
using System.Windows.Forms;

#endregion

namespace Comet.Updater.Client
{
    static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmMain());
        }
    }
}
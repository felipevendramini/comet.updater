﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - Client.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 19:31
// ===================================================================

#endregion

#region References

using System;
using System.Net.Sockets;
using Comet.Updater.Network.Security;
using Comet.Updater.Network.Sockets;

#endregion

namespace Comet.Updater.Client.Communication
{
    public sealed class Client : TcpServerActor
    {
        public Client(Socket socket, Memory<byte> buffer, ICipher cipher)
            : base(socket, buffer, cipher, 0, "Comet.Updater")
        {
        }
    }
}
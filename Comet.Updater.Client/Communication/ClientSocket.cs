﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - ClientSocket.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 19:36
// ===================================================================

#endregion

#region References

using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Comet.Updater.Client.Communication.Packets;
using Comet.Updater.Network.Packets;
using Comet.Updater.Network.Sockets;
using Comet.Updater.Shared;
using Comet.Updater.Shared.Helpers;

#endregion

namespace Comet.Updater.Client.Communication
{
    public sealed class ClientSocket : TcpClientWrapper<Client>
    {
        private readonly PacketProcessor<Client> Processor;

        private const string EXPECTED_FOOTER_S = "Comet.Updater";

        public ClientSocket()
            : base(EXPECTED_FOOTER_S.Length)
        {
            Processor = new PacketProcessor<Client>(ProcessAsync);
            Processor.StartAsync(CancellationToken.None).ConfigureAwait(false);
        }

        protected override async Task<Client> ConnectedAsync(Socket socket, Memory<byte> buffer)
        {
            Client client = new Client(socket, buffer, null);
            if (socket.Connected)
            {
                Kernel.Client = client;

                if (Kernel.ClientStage < Kernel.Stage.Running)
                    Kernel.ClientStage = Kernel.Stage.WaitingForClientUpdates;

                await client.SendAsync(new MsgAutoPatcherSetClientInfo
                {
                    MacAddress = NetworkHelper.GetLocalMacAddress,
                    IpAddresses = NetworkHelper.GetLocalIpAddresses,
                    ComputerName = Environment.MachineName,
                    UserName = Environment.UserName,
                    OperatingSystem = RuntimeInformation.OSDescription
                });
            }
            return client;
        }

        protected override void Received(Client actor, ReadOnlySpan<byte> packet)
        {
            Processor.Queue(actor, packet.ToArray());
        }

        private async Task ProcessAsync(Client actor, byte[] packet)
        {
            // Validate connection
            if (!actor.Socket.Connected)
                return;

            var length = BitConverter.ToUInt16(packet, 0);
            PacketType type = (PacketType) BitConverter.ToUInt16(packet, 2);

            try
            {
                MsgBase<Client> msg = null;

                switch (type)
                {
                    case PacketType.MsgAutoPatcherCheckInfo:
                        msg = new MsgAutoPatcherCheckInfo();
                        break;

                    case PacketType.MsgAutoPatcherPatchInfo:
                        msg = new MsgAutoPatcherPatchInfo();
                        break;

                    case PacketType.MsgAutoPatcherError:
                        msg = new MsgAutoPatcherError();
                        break;

                    default:
                        await Log.WriteLogAsync(LogLevel.Warning,
                            "Missing packet {0}, Length {1}\n{2}",
                            type, length, PacketDump.Hex(packet));
                        return;
                }

                // Decode packet bytes into the structure and process
                msg.Decode(packet);
                await msg.ProcessAsync(actor);
            }
            catch (Exception e)
            {
                await Log.WriteLogAsync(LogLevel.Exception, e.Message);
            }
        }

        protected override void Disconnected(Client actor)
        {
            Kernel.Socket = null;
            Kernel.Client = null;

            Kernel.MainForm.SetProgressStatus(false);
            Kernel.MainForm.SetStatusLabel(LanguageManager.GetString("StrDisconnectedFromServer"));
        }
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - MsgAutoPatcherError.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 19:26
// ===================================================================

#endregion

#region References

using System.Threading.Tasks;
using Comet.Updater.Network.Packets.Structures;

#endregion

namespace Comet.Updater.Client.Communication.Packets
{
    public sealed class MsgAutoPatcherError : MsgAutoPatcherError<Client>
    {
        public override Task ProcessAsync(Client client)
        {
            bool avoidGaming = false;
            string message;
            switch (Error)
            {
                case PatchErrorCode.InvalidClientInformation:
                    message = LanguageManager.GetString("StrInvalidClientInformation");
                    avoidGaming = true;
                    break;

                case PatchErrorCode.ClientAlreadyConnected:
                    message = LanguageManager.GetString("StrClientAlreadyConnected");
                    avoidGaming = true;
                    break;

                case PatchErrorCode.DuplicatedLoginAttempt:
                    message = LanguageManager.GetString("StrDuplicatedLoginAttempt");
                    avoidGaming = true;
                    break;

                case PatchErrorCode.InvalidError:
                    message = LanguageManager.GetString("StrServerInvalidError");
                    break;

                default:
                    message = LanguageManager.GetString("StrUnknownError");
                    break;
            }

            Kernel.MainForm.SetProgressStatus(false);
            Kernel.MainForm.SetStatusLabel(message);
            Kernel.MainForm.SetGameReady(!avoidGaming);
            return Task.CompletedTask;
        }
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - MsgAutoPatcherSetClientInfo.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 16:18
// ===================================================================

#endregion

#region References

using Comet.Updater.Network.Packets.Structures;

#endregion

namespace Comet.Updater.Client.Communication.Packets
{
    public class MsgAutoPatcherSetClientInfo : MsgAutoPatcherSetClientInfo<Client>
    {
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - MsgAutoPatcherCheckInfo.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 20:41
// ===================================================================

#endregion

#region References

using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Comet.Updater.Network.Packets.Structures;

#endregion

namespace Comet.Updater.Client.Communication.Packets
{
    public sealed class MsgAutoPatcherCheckInfo : MsgAutoPatcherCheckInfo<Client>
    {
        public override async Task ProcessAsync(Client client)
        {
            switch (Action)
            {
                case PacketAction.UpdaterVersion:
                {
                    Version = FrmMain.CURRENT_VERSION;
                    await client.SendAsync(this);
                    break;
                }

                case PacketAction.ClientVersion:
                {
                    Version = FrmMain.CurrentGameVersion;
                    await client.SendAsync(this);
                    break;
                }

                case PacketAction.PrivacyDate:
                {
                    if (Values.Count == 0)
                        return;

                    if (!DateTime.TryParse(Values[0], out var privacy))
                        return;

                    if (Kernel.Configuration.TermsOfPrivacy == null || Kernel.Configuration.TermsOfPrivacy < privacy)
                    {
                        
                        break;
                    }
                    await client.SendAsync(this);
                    break;
                }

                case PacketAction.NewUpdateNotify:
                {
                    Kernel.ClientStage = Kernel.Stage.HasUpdatesQueued;

                    if (MessageBox.Show(Kernel.MainForm, LanguageManager.GetString("StrNewUpdateNotification"), "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        Kernel.ClientStage = Kernel.Stage.WaitingForClientUpdates;
                        Action = PacketAction.UpdaterVersion;
                        Version = FrmMain.CURRENT_VERSION;
                        await client.SendAsync(this);
                        break;
                    }

                    Kernel.MainForm.SetStatusLabel(LanguageManager.GetString("StrNewUpdateMessage"));
                    break;
                }

                case PacketAction.UrgentNewUpdateNotify:
                {
                    Kernel.ClientStage = Kernel.Stage.HasUrgentUpdateQueued;

                    if (MessageBox.Show(Kernel.MainForm, LanguageManager.GetString("StrNewUrgentUpdateNotification")) == DialogResult.OK)
                    {
                        Kernel.ClientStage = Kernel.Stage.WaitingForClientUpdates;
                        Action = PacketAction.UpdaterVersion;
                        Version = FrmMain.CURRENT_VERSION;
                        await client.SendAsync(this);
                        break;
                    }

                    Kernel.MainForm.SetStatusLabel(LanguageManager.GetString("StrNewUrgentUpdateNotification"));
                    Kernel.MainForm.Close();
                    break;
                }
            }
        }
    }
}
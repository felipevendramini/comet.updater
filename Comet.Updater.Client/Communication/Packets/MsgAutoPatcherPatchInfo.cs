﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - MsgAutoPatcherPatchInfo.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/23 15:48
// ===================================================================

#endregion

#region References

using System.Threading.Tasks;
using Comet.Updater.Network.Packets;
using Comet.Updater.Network.Packets.Structures;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Client.Communication.Packets
{
    public sealed class MsgAutoPatcherPatchInfo : MsgAutoPatcherPatchInfo<Client>
    {
        public override async Task ProcessAsync(Client client)
        {
            switch (Kernel.ClientStage)
            {
                case Kernel.Stage.WaitingForClientUpdates:
                {
                    if (LatestVersion == 0 || Count == 0)
                    {
                        await client.SendAsync(new MsgAutoPatcherCheckInfo
                        {
                            Action = MsgAutoPatcherCheckInfo<Client>.PacketAction.ClientVersion,
                            Version = FrmMain.CurrentGameVersion
                        });

                        Kernel.ClientStage = Kernel.Stage.WaitingForGameClientUpdates;
                        return;
                    }

                    if (LatestVersion < 10000)
                    {
                        return;
                    }
                    break;
                }

                case Kernel.Stage.WaitingForGameClientUpdates:
                {
                    if (LatestVersion == 0 || Count == 0)
                    {
                        Kernel.ClientStage = Kernel.Stage.TermsOfPrivacy;
                        return;
                    }

                    if (LatestVersion > 9999)
                    {
                        return;
                    }
                    break;
                }

                case Kernel.Stage.TermsOfPrivacy:
                {
                    break;
                }

                default:
                {
                    await Log.WriteLogAsync(LogLevel.Cheat, $"Invalid client stage [{Kernel.ClientStage}] for updating.");
                    await Log.WriteLogAsync(LogLevel.Socket, PacketDump.Hex(Encode()));
                    return;
                }
            }

            await Kernel.MainForm.ProcessDownloadAsync(Domain, LatestVersion, Updates);
        }
    }
}
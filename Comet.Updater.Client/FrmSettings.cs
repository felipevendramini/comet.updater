﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - FrmSettings.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/23 11:27
// ===================================================================

#endregion

#region References

using System;
using System.Drawing;
using System.Windows.Forms;
using Comet.Updater.Client.Configuration;
using Comet.Updater.Client.Properties;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Client
{
    public partial class FrmSettings : Form
    {
        public FrmSettings()
        {
            InitializeComponent();

            SuspendLayout();

            AllowTransparency = true;
            TransparencyKey = Color.FromArgb(0xff00d8);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            BackColor = Color.FromArgb(0x00FFFFFF);
            
            btnAccept.FlatAppearance.MouseOverBackColor = Color.Transparent;
            btnAccept.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnCancel.FlatAppearance.MouseDownBackColor = Color.Transparent;
            btnCancel.FlatAppearance.MouseOverBackColor = Color.Transparent;

            ResumeLayout();
        }

        private void FrmSettings_Load(object sender, EventArgs e)
        {
            Text = LanguageManager.GetString(Text);

            lblFps.Text = LanguageManager.GetString(lblFps.Text);
            lblScreenResolution.Text = LanguageManager.GetString(lblScreenResolution.Text);
            lnkDefaultFps.Text = LanguageManager.GetString(lnkDefaultFps.Text);
            lnkDefaultResolution.Text = LanguageManager.GetString(lnkDefaultResolution.Text);
            btnAccept.Text = LanguageManager.GetString(btnAccept.Text);
            btnCancel.Text = LanguageManager.GetString(btnCancel.Text);

            if (UserConfigurationManager.Configuration == null)
            {
                numFps.Value = UserConfigurationManager.DEFAULT_FPS_I;
                numScreenWidth.Value = 1024;
                numScreenHeight.Value = 768;
                return;
            }

            numFps.Value = UserConfigurationManager.Configuration.FramesPerSecond;
            numScreenWidth.Value = UserConfigurationManager.Configuration.ScreenWidth;
            numScreenHeight.Value = UserConfigurationManager.Configuration.ScreenHeight;
        }

        private void lnkDefaultFps_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            numFps.Value = UserConfigurationManager.DEFAULT_FPS_I;
        }

        private void lnkDefaultResolution_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (UserConfigurationManager.Configuration == null)
            {
                numScreenWidth.Value = 1024;
                numScreenHeight.Value = 768;
                return;
            }

            numScreenWidth.Value = UserConfigurationManager.Configuration.MaxScreenWidth;
            numScreenHeight.Value = UserConfigurationManager.Configuration.MaxScreenHeight;
        }

        private void btnCancel_MouseDown(object sender, MouseEventArgs e)
        {
            BackgroundImage = Resources.MoreClicked;
        }

        private void btnCancel_MouseUp(object sender, MouseEventArgs e)
        {
            BackgroundImage = Resources.More;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            XmlParser parser = new XmlParser(UserConfigurationManager.FILE_S);
            parser.AddNewNode(numFps.Value, null, "FramesPerSecond", "Config");
            parser.AddNewNode(numScreenWidth.Value, null, "Width", "Config", "Resolution");
            parser.AddNewNode(numScreenHeight.Value, null, "Height", "Config", "Resolution");
            parser.Save();

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Client - Kernel.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/02/05 13:07
// ===================================================================

#endregion

#region References

using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Comet.Updater.Client.Communication;
using Comet.Updater.Client.Configuration;

#endregion

namespace Comet.Updater.Client
{
    public static class Kernel
    {
        public static Stage ClientStage;

        public static ConfigurationManager Configuration;

        public static FrmMain MainForm => (FrmMain) Application.OpenForms[0];

        public static ClientSocket Socket;
        public static Communication.Client Client;

        public static void OpenUrl(string url)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                url = url.Replace("&", "^&");
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Process.Start("xdg-open", url);
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                Process.Start("open", url);
            }
        }

        public enum Stage
        {
            /// <summary>
            /// The client has just started.
            /// </summary>
            None,
            /// <summary>
            /// The client has started and is now waiting for client updates.
            /// </summary>
            WaitingForClientUpdates,
            /// <summary>
            /// The client has downloaded all client updates and now can update the game client.
            /// </summary>
            WaitingForGameClientUpdates,
            TermsOfPrivacy,
            /// <summary>
            /// The client still can receive updates notifications, but it will ask for permission to download data.
            /// </summary>
            Running,
            /// <summary>
            /// The client has received updates notification. After confirming the client will roll back to WaitingForClientUpdates
            /// and request the incoming updates.
            /// </summary>
            HasUpdatesQueued,
            /// <summary>
            /// The client has received an urgent update notification. After confirming the client will close all game clients.
            /// The client will roll back to WaitingForClientUpdates and request the incoming updates.
            /// </summary>
            HasUrgentUpdateQueued
        }
    }
}
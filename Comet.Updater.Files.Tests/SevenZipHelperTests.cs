﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Files.Tests - SevenZipHelperTests.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/24 18:09
// ===================================================================

#endregion

#region References

using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace Comet.Updater.Files.Tests
{
    [TestClass]
    public class SevenZipHelperTests
    {
        [TestMethod]
        public void Extract7ZipFileTest()
        {
            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "PatchExtract")))
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "PatchExtract"));

            //SevenZipHelper.Extract7ZipFile(Path.Combine(Environment.CurrentDirectory, "Files", "09christmas03.7z"), Path.Combine(Environment.CurrentDirectory, "PatchExtract"));
            SevenZipHelper.Extract7ZipFile(Path.Combine(Environment.CurrentDirectory, "Files", "test.7z"), Path.Combine(Environment.CurrentDirectory, "PatchExtract"));
        }
    }
}
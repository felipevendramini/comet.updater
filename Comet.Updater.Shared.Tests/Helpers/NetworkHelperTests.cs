﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Shared.Tests - NetworkHelperTests.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 19:32
// ===================================================================

#endregion

#region References

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace Comet.Updater.Shared.Helpers.Tests
{
    [TestClass]
    public class NetworkHelperTests
    {
        [TestMethod]
        public void IsValidMacAddressTest()
        {
            string[] validMacAddressesArray = {"000C29C9C01B",
                "000000000000",
                "AAAAAAAAAAAA",
                "14:DA:E9:41:F0:D6",
                "14-DA-E9-41-F0-D6",
                "14:DA:E9:41:F0:D6",
                "14-DA-E9-41-F0:D6"};

            string[] invalidMacAddressesArray = {"000",
                "AAA",
                "ZZZ",
                "ZZZZZZZZZZZZ",
                "ZZ:ZZ:E9:41:F0:D6"};

            foreach (var valid in validMacAddressesArray)
            {
                Assert.IsTrue(NetworkHelper.IsValidMacAddress(valid));
            }

            foreach (var invalid in invalidMacAddressesArray)
            {
                Assert.IsFalse(NetworkHelper.IsValidMacAddress(invalid));
            }

            Assert.IsTrue(NetworkHelper.IsValidMacAddress(NetworkHelper.GetLocalMacAddress));
        }
    }
}
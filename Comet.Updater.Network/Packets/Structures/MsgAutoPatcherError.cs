﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Network - MsgAutoPatcherError.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 19:13
// ===================================================================

#endregion

namespace Comet.Updater.Network.Packets.Structures
{
    public abstract class MsgAutoPatcherError<T> : MsgBase<T>
    {
        public enum PatchErrorCode
        {
            InvalidError,
            UnknownError = 100000,
            InvalidClientInformation,
            ClientAlreadyConnected,
            DuplicatedLoginAttempt
        }

        public PatchErrorCode Error;

        public override void Decode(byte[] bytes)
        {
            PacketReader reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType) reader.ReadUInt16();
            Error = (PatchErrorCode) reader.ReadInt32();
        }

        public override byte[] Encode()
        {
            PacketWriter writer = new PacketWriter();
            writer.Write((ushort) PacketType.MsgAutoPatcherError);
            writer.Write((int) Error);
            return writer.ToArray();
        }
    }
}
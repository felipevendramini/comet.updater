﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Network - MsgLoginServerSyncServer.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 18:42
// ===================================================================

#endregion

#region References

using System.Collections.Generic;
using System.Linq;

#endregion

namespace Comet.Updater.Network.Packets.Structures
{
    public abstract class MsgLoginServerSyncServer<T> : MsgBase<T>
    {
        public int Count;
        public List<ServerInfoStruct> Servers = new List<ServerInfoStruct>();

        public override void Decode(byte[] bytes)
        {
            PacketReader reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType) reader.ReadUInt16();
            Count = reader.ReadInt32();

            for (int i = 0; i < Count; i++)
            {
                int idServer = reader.ReadInt32();
                string name = reader.ReadString(16);
                int port = reader.ReadInt32();
                int onlinePlayers = reader.ReadInt32();
                int maxOnlinePlayers = reader.ReadInt32();
                int playerLimit = reader.ReadInt32();

                Servers.Add(new ServerInfoStruct
                {
                    MaximumOnlinePlayers = maxOnlinePlayers,
                    OnlinePlayers = onlinePlayers,
                    PlayerLimit = playerLimit,
                    ServerId = idServer,
                    ServerName = name,
                    ServerPort = port
                });
            }
        }

        public override byte[] Encode()
        {
            PacketWriter writer = new PacketWriter();
            writer.Write((ushort) PacketType.MsgLoginServerSyncServer);
            writer.Write(Count = Servers.Count);
            foreach (var server in Servers.OrderBy(x => x.ServerId))
            {
                writer.Write(server.ServerId);
                writer.Write(server.ServerName, 16);
                writer.Write(server.ServerPort);
                writer.Write(server.OnlinePlayers);
                writer.Write(server.MaximumOnlinePlayers);
                writer.Write(server.PlayerLimit);
            }

            return writer.ToArray();
        }

        public struct ServerInfoStruct
        {
            public int ServerId;
            public string ServerName;
            public int ServerPort;
            public int OnlinePlayers;
            public int MaximumOnlinePlayers;
            public int PlayerLimit;
        }
    }
}
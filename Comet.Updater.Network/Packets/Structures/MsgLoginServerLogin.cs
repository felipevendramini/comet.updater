﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Network - MsgLoginServerLogin.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 18:39
// ===================================================================

#endregion

namespace Comet.Updater.Network.Packets.Structures
{
    public abstract class MsgLoginServerLogin<T> : MsgBase<T>
    {
        public string Password;
        public string Username;

        public override void Decode(byte[] bytes)
        {
            PacketReader reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType) reader.ReadUInt16();
            Username = reader.ReadString(128);
            Password = reader.ReadString(256);
        }

        public override byte[] Encode()
        {
            PacketWriter writer = new PacketWriter();
            writer.Write((ushort) PacketType.MsgLoginServerLogin);
            writer.Write(Username, 128);
            writer.Write(Password, 256);
            return writer.ToArray();
        }
    }
}
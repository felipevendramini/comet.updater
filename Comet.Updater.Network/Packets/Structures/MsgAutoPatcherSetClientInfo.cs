﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Network - MsgAutoPatcherSetClientInfo.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 18:38
// ===================================================================

#endregion

using System.Collections.Generic;

namespace Comet.Updater.Network.Packets.Structures
{
    public abstract class MsgAutoPatcherSetClientInfo<T> : MsgBase<T>
    {
        public string ComputerName;
        public string MacAddress;
        public string UserName;
        public string OperatingSystem;
        public List<string> IpAddresses = new List<string>();

        public override void Decode(byte[] bytes)
        {
            PacketReader reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType) reader.ReadUInt16();
            MacAddress = reader.ReadString(16);
            ComputerName = reader.ReadString(128);
            UserName = reader.ReadString(128);
            OperatingSystem = reader.ReadString();
            IpAddresses = reader.ReadStrings();
        }

        public override byte[] Encode()
        {
            PacketWriter writer = new PacketWriter();
            writer.Write((ushort) PacketType.MsgAutoPatcherSetClientInfo);
            writer.Write(MacAddress, 16);
            writer.Write(ComputerName, 128);
            writer.Write(UserName, 128);
            writer.Write(OperatingSystem);
            writer.Write(IpAddresses);
            return writer.ToArray();
        }
    }
}
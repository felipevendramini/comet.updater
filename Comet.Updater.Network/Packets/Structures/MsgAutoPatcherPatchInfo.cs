﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Network - MsgAutoPatcherPatchInfo.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/16 14:10
// ===================================================================

#endregion

#region References

using System.Collections.Generic;

#endregion

namespace Comet.Updater.Network.Packets.Structures
{
    public abstract class MsgAutoPatcherPatchInfo<T> : MsgBase<T>
    {
        public string Domain;
        public int LatestVersion;
        public int Count;
        public List<UpdateInfo> Updates = new List<UpdateInfo>();

        public override void Decode(byte[] bytes)
        {
            PacketReader reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType) reader.ReadUInt16();
            Domain = reader.ReadString(128);
            LatestVersion = reader.ReadInt32();
            Count = reader.ReadInt32();
            for (int i = 0; i < Count; i++)
            {
                int to = reader.ReadInt32();
                //byte length = reader.ReadByte();
                string file = reader.ReadString();
                Updates.Add(new UpdateInfo
                {
                    To = to,
                    File = file
                });
            }
        }

        public override byte[] Encode()
        {
            PacketWriter writer = new PacketWriter();
            writer.Write((ushort) PacketType.MsgAutoPatcherPatchInfo);
            writer.Write(Domain, 128);
            writer.Write(LatestVersion);
            writer.Write(Count = Updates.Count);
            foreach (var update in Updates)
            {
                writer.Write(update.To);
                writer.Write(update.File);
            }
            return writer.ToArray();
        }

        public struct UpdateInfo
        {
            public int To;
            public string File;
        }
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Network - MsgAutoPatcherPing.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/23 15:52
// ===================================================================

#endregion

namespace Comet.Updater.Network.Packets.Structures
{
    public abstract class MsgAutoPatcherPing<T> : MsgBase<T>
    {
        public int CheckSum;
        public int OpenClients;
        public int Time;

        public override void Decode(byte[] bytes)
        {
            using PacketReader reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType) reader.ReadUInt16();
            Time = reader.ReadInt32();
            OpenClients = reader.ReadInt32();
            CheckSum = reader.ReadInt32();
        }

        public override byte[] Encode()
        {
            using PacketWriter writer = new PacketWriter();
            writer.Write((ushort) PacketType.MsgAutoPatcherPing);
            writer.Write(Time);
            writer.Write(OpenClients);
            writer.Write(CheckSum);
            return writer.ToArray();
        }
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Network - MsgAutoPatcherCheckInfo.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/16 14:03
// ===================================================================

#endregion

using System.Collections.Generic;

namespace Comet.Updater.Network.Packets.Structures
{
    public abstract class MsgAutoPatcherCheckInfo<T> : MsgBase<T>
    {
        public PacketAction Action;
        public int Version;
        public List<string> Values = new List<string>();

        public override void Decode(byte[] bytes)
        {
            PacketReader reader = new PacketReader(bytes);
            Length = reader.ReadUInt16();
            Type = (PacketType) reader.ReadUInt16();
            Action = (PacketAction) reader.ReadInt32();
            Version = reader.ReadInt32();
            Values = reader.ReadStrings();
        }

        public override byte[] Encode()
        {
            PacketWriter writer = new PacketWriter();
            writer.Write((ushort) PacketType.MsgAutoPatcherCheckInfo);
            writer.Write((int) Action);
            writer.Write(Version);
            writer.Write(Values);
            return writer.ToArray();
        }

        public enum PacketAction
        {
            UpdaterVersion,
            ClientVersion,
            PrivacyDate,
            NewUpdateNotify,
            UrgentNewUpdateNotify
        }
    }
}
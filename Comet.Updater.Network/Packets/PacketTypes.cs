﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Network - PacketTypes.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/02/05 13:16
// ===================================================================

#endregion

namespace Comet.Updater.Network.Packets
{
    /// <summary>
    ///     Packet types for the Conquer Online game client across all server projects.
    ///     Identifies packets by an unsigned short from offset 2 of every packet sent to
    ///     the server.
    /// </summary>
    public enum PacketType : ushort
    {
        MsgAutoPatcherBegin = 1000,
        MsgAutoPatcherCheckInfo,
        MsgAutoPatcherPatchInfo,
        MsgAutoPatcherSetClientInfo,
        MsgAutoPatcherError,
        MsgAutoPatcherPing,
        MsgAutoPatcherEnd = 9999,

        MsgLoginServerBegin = 10000,
        MsgLoginServerLogin,
        MsgLoginServerSyncServer,
        MsgLoginServerEnd = 19999,

        MsgInterServerBegin = 20000,
        MsgInterServerEnd = 29999
    }
}
﻿#region References

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Comet.Updater.Network.Packets;
using Comet.Updater.Network.Security;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Network.Sockets
{
    /// <summary>
    ///     Actors are assigned to accepted client sockets to give connected clients a state
    ///     across socket operations. This allows the server to handle multiple receive writes
    ///     across single processing reads, and keep a buffer alive for faster operations.
    /// </summary>
    public abstract class TcpServerActor
    {
        // Fields and Properties
        public readonly Memory<byte> Buffer;
        public readonly ICipher Cipher;
        public readonly byte[] PacketFooter;
        public readonly uint Partition;
        private readonly object SendLock;
        public readonly Socket Socket;

        public bool Exchanged = false;

        /// <summary>
        ///     Instantiates a new instance of <see cref="TcpServerActor" /> using an accepted
        ///     client socket and preallocated buffer from the server listener.
        /// </summary>
        /// <param name="socket">Accepted client socket</param>
        /// <param name="buffer">Preallocated buffer for socket receive operations</param>
        /// <param name="cipher">Cipher for handling client encipher operations</param>
        /// <param name="partition">Packet processing partition, default is disabled</param>
        protected TcpServerActor(
            Socket socket,
            Memory<byte> buffer,
            ICipher cipher,
            uint partition = 0,
            string packetFooter = "")
        {
            Buffer = buffer;
            Cipher = cipher;
            Socket = socket;
            PacketFooter = Encoding.ASCII.GetBytes(packetFooter);
            Partition = partition;
            SendLock = new object();

            IPAddress = (Socket.RemoteEndPoint as IPEndPoint)?.Address.MapToIPv4().ToString();
        }

        /// <summary>
        ///     Returns the remote IP address of the connected client.
        /// </summary>
        public string IPAddress { get; }

        /// <summary>
        ///     Sends a packet to the game client after encrypting bytes. This may be called
        ///     as-is, or overridden to provide channel functionality and thread-safety around
        ///     the accepted client socket. By default, this method locks around encryption
        ///     and sending data.
        /// </summary>
        /// <param name="packet">Bytes to be encrypted and sent to the client</param>
        public virtual Task<int> SendAsync(byte[] packet)
        {
            var encrypted = new byte[packet.Length + PacketFooter.Length];
            packet.CopyTo(encrypted, 0);

            BitConverter.TryWriteBytes(encrypted, (ushort) packet.Length);
            Array.Copy(PacketFooter, 0, encrypted, packet.Length, PacketFooter.Length);

            lock (SendLock)
            {
                try
                {
                    if (Socket?.Connected != true)
                        return Task.FromResult(-1);

                    Cipher?.Encrypt(encrypted, encrypted);
                    return Socket.SendAsync(encrypted, SocketFlags.None);
                }
                catch (SocketException e)
                {
                    if (e.SocketErrorCode < SocketError.ConnectionAborted ||
                        e.SocketErrorCode > SocketError.Shutdown)
                        Console.WriteLine(e);
                    return Task.FromResult(-1);
                }
                catch (Exception ex)
                {
                    Log.WriteLogAsync("TcpServerActor-SendAsync", LogLevel.Exception, ex.ToString())
                        .ConfigureAwait(false);
                    return Task.FromResult(-1);
                }
            }
        }

        /// <summary>
        ///     Sends a packet to the game client after encrypting bytes. This may be called
        ///     as-is, or overridden to provide channel functionality and thread-safety around
        ///     the accepted client socket. By default, this method locks around encryption
        ///     and sending data.
        /// </summary>
        /// <param name="packet">Packet to be encrypted and sent to the client</param>
        public virtual Task<int> SendAsync(IPacket packet)
        {
            return SendAsync(packet.Encode());
        }

        /// <summary>
        ///     Force closes the client connection.
        /// </summary>
        public virtual void Disconnect()
        {
            Socket?.Disconnect(false);
        }
    }
}
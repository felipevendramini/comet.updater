﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - Server.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 15:06
// ===================================================================

#endregion

#region References

using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Comet.Updater.Network.Packets;
using Comet.Updater.Network.Sockets;
using Comet.Updater.Server.Packets;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Server
{
    internal sealed class Server : TcpServerListener<Client>
    {
        private const string EXPECTED_FOOTER_S = "Comet.Updater";

        // Fields and Properties
        private readonly PacketProcessor<Client> Processor;

        public Server()
            : base(5000, 1024, false, false, EXPECTED_FOOTER_S.Length)
        {
            Processor = new PacketProcessor<Client>(ProcessAsync);
            Processor.StartAsync(CancellationToken.None).ConfigureAwait(false);
        }

        protected override async Task<Client> AcceptedAsync(Socket socket, Memory<byte> buffer)
        {
            Client client = new Client(socket, buffer, Processor.SelectPartition());
            return client;
        }

        /// <summary>
        ///     Invoked by the server listener's Receiving method to process a completed packet
        ///     from the actor's socket pipe. At this point, the packet has been assembled and
        ///     split off from the rest of the buffer.
        /// </summary>
        /// <param name="actor">Server actor that represents the remote client</param>
        /// <param name="packet">Packet bytes to be processed</param>
        protected override void Received(Client actor, ReadOnlySpan<byte> packet)
        {
            Processor.Queue(actor, packet.ToArray());
        }

        /// <summary>
        ///     Invoked by one of the server's packet processor worker threads to process a
        ///     single packet of work. Allows the server to process packets as individual
        ///     messages on a single channel.
        /// </summary>
        /// <param name="actor">Actor requesting packet processing</param>
        /// <param name="packet">An individual data packet to be processed</param>
        private async Task ProcessAsync(Client actor, byte[] packet)
        {
            // Validate connection
            if (!actor.Socket.Connected)
                return;

            var length = BitConverter.ToUInt16(packet, 0);
            PacketType type = (PacketType) BitConverter.ToUInt16(packet, 2);

            try
            {
                MsgBase<Client> msg = null;

                switch (type)
                {
                    case PacketType.MsgAutoPatcherCheckInfo:
                        msg = new MsgAutoPatcherCheckInfo();
                        break;

                    case PacketType.MsgAutoPatcherSetClientInfo:
                        msg = new MsgAutoPatcherSetClientInfo();
                        break;

                    default:
                        await Log.WriteLogAsync(LogLevel.Warning,
                            "Missing packet {0}, Length {1}\n{2}",
                            type, length, PacketDump.Hex(packet));
                        return;
                }

                // Decode packet bytes into the structure and process
                msg.Decode(packet);
                await msg.ProcessAsync(actor);
            }
            catch (Exception e)
            {
                await Log.WriteLogAsync(LogLevel.Error, e.ToString());
            }
        }

        protected override void Disconnected(Client actor)
        {
            if (actor == null)
                return;

            Log.WriteLogAsync(LogLevel.Message, $"Client [Partition: {actor.Partition}] [{actor.IPAddress}][{actor.ComputerName}>{actor.MacAddress}] has disconnected.").ConfigureAwait(false);

            Kernel.Clients.TryRemove(actor.Key, out _);

            // todo make server disconnect players
        }
    }
}
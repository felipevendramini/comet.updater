﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - Client.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 15:58
// ===================================================================

#endregion

#region References

using System;
using System.Collections.Generic;
using System.Net.Sockets;
using Comet.Updater.Network.Sockets;

#endregion

namespace Comet.Updater.Server
{
    public sealed class Client : TcpServerActor
    {
        /// <summary>
        ///     Instantiates a new instance of <see cref="Client" /> using the Accepted event's
        ///     resulting socket and preallocated buffer. Initializes all account server
        ///     states, such as the cipher used to decrypt and encrypt data.
        /// </summary>
        /// <param name="socket">Accepted remote client socket</param>
        /// <param name="buffer">Preallocated buffer from the server listener</param>
        /// <param name="partition">Packet processing partition</param>
        public Client(Socket socket, Memory<byte> buffer, uint partition)
            : base(socket, buffer, null, partition, "Comet.Updater")
        {
        }

        public string ComputerName;
        public string MacAddress;
        public string UserName;
        public List<string> IpAddresses = new List<string>();
        public int Key => $"{ComputerName.ToUpperInvariant()}-{MacAddress}".GetHashCode();
    }
}
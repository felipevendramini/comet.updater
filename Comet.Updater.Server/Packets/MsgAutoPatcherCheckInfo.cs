﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - MsgAutoPatcherCheckInfo.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 16:11
// ===================================================================

#endregion

#region References

using System.Collections.Generic;
using System.Threading.Tasks;
using Comet.Updater.Network.Packets.Structures;
using Comet.Updater.Server.Structures;

#endregion

namespace Comet.Updater.Server.Packets
{
    public sealed class MsgAutoPatcherCheckInfo : MsgAutoPatcherCheckInfo<Client>
    {
        public override async Task ProcessAsync(Client client)
        {
            List<UpdateStruct> updates = null;
            int version = 0;
            switch (Action)
            {
                case PacketAction.UpdaterVersion:
                {
                    updates = Kernel.Config.GetUpdateSequence(Version, false);
                    version = Kernel.Config.LatestClientUpdate();
                    break;
                }

                case PacketAction.ClientVersion:
                {
                    updates = Kernel.Config.GetUpdateSequence(Version, true);
                    version = Kernel.Config.LatestGameClientUpdate();
                    break;
                }

                case PacketAction.PrivacyDate:
                {
                    /*
                     * If Version is different than zero, so we will conclude that the user has accepted the Privacy Agreement.
                     * Otherwise, we'll tell the account server to lock any login attempts from the current machine.
                     */
                    if (Version != 0) // nothing changed
                        return;

                    // TODO: sync with account server to disconnect or not allow logins
                    return;
                }

                case PacketAction.UrgentNewUpdateNotify:
                {
                    return; // TODO: enable the client to login
                }
            }

            if (updates == null)
            {
                return;
            }

            MsgAutoPatcherPatchInfo msg = new MsgAutoPatcherPatchInfo
            {
                Domain = Kernel.Config.DownloadFrom,
                LatestVersion = version
            };
            foreach (var update in updates)
            {
                if (update.To == 0)
                    continue;

                msg.Updates.Add(new MsgAutoPatcherPatchInfo<Client>.UpdateInfo
                {
                    To = update.To,
                    File = update.FullFileName
                });
            }
            await client.SendAsync(msg);
        }
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - MsgAutoPatcherError.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 19:26
// ===================================================================

#endregion

#region References

using Comet.Updater.Network.Packets.Structures;

#endregion

namespace Comet.Updater.Server.Packets
{
    public sealed class MsgAutoPatcherError : MsgAutoPatcherError<Client>
    {
    }
}
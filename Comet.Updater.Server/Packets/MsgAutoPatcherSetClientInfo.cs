﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - MsgAutoPatcherSetClientInfo.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/22 18:35
// ===================================================================

#endregion

#region References

using System.Collections.Generic;
using System.Threading.Tasks;
using Comet.Updater.Network.Packets.Structures;
using Comet.Updater.Shared;
using Comet.Updater.Shared.Helpers;

#endregion

namespace Comet.Updater.Server.Packets
{
    public sealed class MsgAutoPatcherSetClientInfo : MsgAutoPatcherSetClientInfo<Client>
    {
        public override async Task ProcessAsync(Client client)
        {
            if (string.IsNullOrEmpty(MacAddress) || !NetworkHelper.IsValidMacAddress(MacAddress))
            {
                await client.SendAsync(new MsgAutoPatcherError
                {
                    Error = MsgAutoPatcherError<Client>.PatchErrorCode.InvalidClientInformation
                });
                client.Disconnect();
                return;
            }

            if (string.IsNullOrEmpty(ComputerName))
            {
                await client.SendAsync(new MsgAutoPatcherError
                {
                    Error = MsgAutoPatcherError<Client>.PatchErrorCode.InvalidClientInformation
                });
                client.Disconnect();
                return;
            }

            if (string.IsNullOrEmpty(UserName))
            {
                await client.SendAsync(new MsgAutoPatcherError
                {
                    Error = MsgAutoPatcherError<Client>.PatchErrorCode.InvalidClientInformation
                });
                client.Disconnect();
                return;
            }

            if (IpAddresses.Count == 0)
            {
                await client.SendAsync(new MsgAutoPatcherError
                {
                    Error = MsgAutoPatcherError<Client>.PatchErrorCode.InvalidClientInformation
                });
                client.Disconnect();
                return;
            }

            client.MacAddress = MacAddress;
            client.ComputerName = ComputerName;
            client.UserName = UserName;
            client.IpAddresses = new List<string>(IpAddresses);

            if (Kernel.Clients.TryGetValue(client.Key, out var logged))
            {
                await client.SendAsync(new MsgAutoPatcherError
                {
                    Error = MsgAutoPatcherError<Client>.PatchErrorCode.ClientAlreadyConnected
                });
                client.Disconnect();

                await logged.SendAsync(new MsgAutoPatcherError
                {
                    Error = MsgAutoPatcherError<Client>.PatchErrorCode.DuplicatedLoginAttempt
                });
                logged.Disconnect();
                return;
            }

            Kernel.Clients.TryAdd(client.Key, client);

            await Log.WriteLogAsync(LogLevel.Message, $"Client [Partition: {client.Partition}] [{client.IPAddress}][{client.ComputerName}>{client.MacAddress}] has been connected.");

            await client.SendAsync(new MsgAutoPatcherCheckInfo
            {
                Action = MsgAutoPatcherCheckInfo<Client>.PacketAction.UpdaterVersion
            });
        }
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - UpdatesConfig.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/16 13:42
// ===================================================================

#endregion

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Comet.Updater.Server.Structures;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Server.Configuration
{
    public sealed class UpdatesConfig
    {
        public static readonly string ConfigFilePath;
        private ConcurrentDictionary<int, UpdateStruct> mUpdatesDictionary;

        private XmlParser mXmlParser;

        static UpdatesConfig()
        {
            ConfigFilePath = Path.Combine(Environment.CurrentDirectory, "Config.xml");
        }

        private UpdatesConfig()
        {
            mUpdatesDictionary = new ConcurrentDictionary<int, UpdateStruct>();
        }

        public int ListenPort { get; private set; }
        public string DownloadFrom { get; private set; }

        public static UpdatesConfig Create()
        {
            UpdatesConfig config = new UpdatesConfig();

            if (!File.Exists(ConfigFilePath))
                config.mXmlParser = CreateConfigFile(ConfigFilePath);
            else config.mXmlParser = new XmlParser(ConfigFilePath);

            config.LoadConfig();
            return config;
        }

        private static XmlParser CreateConfigFile(string location)
        {
            XmlTextWriter writer = new XmlTextWriter(location, Encoding.UTF8)
            {
                Formatting = Formatting.Indented
            };
            writer.WriteStartDocument();
            writer.WriteStartElement("Config");
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();

            var parser = new XmlParser(location);
            parser.AddNewNode("http://localhost/patches", null, "Url", "Config");
            parser.AddNewNode("9528", null, "ListenPort", "Config");
            parser.AddNewNode("2000-01-01T00:00:00.000-0300", null, "PrivacyTerms", "Config");
            parser.AddNewNode("", null, "Updates", "Config");
            return parser;
        }

        public void LoadConfig()
        {
            try
            {
                ListenPort = int.Parse(mXmlParser.GetValue("Config", "ListenPort"));
                DownloadFrom = mXmlParser.GetValue("Config", "Url");
                foreach (XmlNode node in mXmlParser.GetAllNodes("Config", "Updates"))
                {
                    string id = node.Attributes?["id"]?.Value;
                    if (string.IsNullOrEmpty(id))
                        continue;

                    int from = 0;
                    int to = 0;
                    string ext = mXmlParser.GetValue("Config", "Updates", $"Patch[@id='{id}']", "Extension");

                    if (!int.TryParse(
                        mXmlParser.GetValue("Config", "Updates", $"Patch[@id='{node.Attributes["id"].Value}']", "From"),
                        out from))
                    {
                        _ = Log.WriteLogAsync(LogLevel.Error, $"Invalid patch [From] data for ID '{id}'.");
                        continue;
                    }

                    if (!int.TryParse(
                        mXmlParser.GetValue("Config", "Updates", $"Patch[@id='{node.Attributes["id"].Value}']", "To"),
                        out to))
                        to = @from;

                    if (!SharedKernel.IsSupportedExt(ext))
                    {
                        _ = Log.WriteLogAsync(LogLevel.Error, $"Invalid extension [{ext}] for ID '{id}'.");
                        continue;
                    }

                    var str = new UpdateStruct
                    {
                        From = from,
                        To = to,
                        Extension = ext
                    };
                    if (mUpdatesDictionary.TryAdd(from, str))
                    {
                        if (str.IsBundle)
                            _ = Log.WriteLogAsync(LogLevel.Message, $"Bundle [{str.FullFileName}] loaded.");
                        else
                            _ = Log.WriteLogAsync(LogLevel.Message, $"Patch [{str.FullFileName}] loaded.");
                    }
                    else
                    {
                        _ = Log.WriteLogAsync(LogLevel.Error, $"Possible duplicate of patch ID -> '{id}'.");
                    }
                }
            }
            catch (Exception e)
            {
                _ = Log.WriteLogAsync(LogLevel.Exception, e.Message);
            }
        }

        public bool AppendPatch(UpdateStruct patch)
        {
            if (patch.From == 0)
                return false;

            if (mUpdatesDictionary.ContainsKey(patch.From))
                return false;

            if (mUpdatesDictionary.TryAdd(patch.From, patch))
            {
                mXmlParser.AddNewNode("", patch.From.ToString(), "Patch", "Config", "Updates");
                mXmlParser.AddNewNode($"{patch.From}", null, "From", "Config", "Updates", $"Patch[@id='{patch.From}']");
                mXmlParser.AddNewNode($"{patch.To}", null, "To", "Config", "Updates", $"Patch[@id='{patch.From}']");
                mXmlParser.AddNewNode($"{patch.Extension}", null, "Extension", "Config", "Updates",
                    $"Patch[@id='{patch.From}']");
                mXmlParser.Save();
                return true;
            }

            return false;
        }

        public bool ContainsUpdate(int from)
        {
            return mUpdatesDictionary.ContainsKey(from);
        }

        public bool RemovePatch(int from)
        {
            if (from == 0)
                return false;
            if (mUpdatesDictionary.TryRemove(from, out var patch))
            {
                mXmlParser.DeleteNode("Config", "Updates", $"Patch[@id='{from}']");
                return true;
            }

            return false;
        }

        public int LatestClientUpdate()
        {
            return mUpdatesDictionary.Values.Where(x => x.IsUpdate).DefaultIfEmpty(new UpdateStruct()).Max(x => x.To);
        }

        public int LatestGameClientUpdate()
        {
            return mUpdatesDictionary.Values.Where(x => !x.IsUpdate).DefaultIfEmpty(new UpdateStruct()).Max(x => x.To);
        }

        public List<UpdateStruct> GetUpdateSequence(int from, bool game)
        {
            var result = new List<UpdateStruct>();
            int current = game ? LatestGameClientUpdate() : LatestClientUpdate();

            if (!game)
                return new List<UpdateStruct>
                {
                    mUpdatesDictionary.Values.Where(x => x.IsUpdate).OrderByDescending(x => x.To).FirstOrDefault()
                };

            foreach (var patch in mUpdatesDictionary.Values
                .Where(x => x.IsUpdate == false && x.From > from)
                .OrderByDescending(x => x.To))
            {
                if (patch.To > current)
                    continue;

                result.Add(patch);

                if (patch.IsBundle)
                    current = patch.From;
                else current -= 1;
            }

            return result.OrderBy(x => x.To).ToList();
        }

        public void ListUpdates()
        {
            Console.WriteLine(
                $"Last Client Update[{LatestClientUpdate():00000}] \tLast Game Client Update[{LatestGameClientUpdate():0000}]");

            Console.WriteLine("Displaying Updates");
            Console.WriteLine("Auto Patcher Client");
            foreach (var update in mUpdatesDictionary.Values.Where(x => x.IsUpdate).OrderBy(x => x.From))
                Console.WriteLine($"\tUpdate {update.FileName}");

            Console.WriteLine("Game Client");
            foreach (var update in mUpdatesDictionary.Values.Where(x => !x.IsUpdate).OrderBy(x => x.From))
                if (!update.IsBundle)
                    Console.WriteLine($"\tUpdate {update.FileName}");
                else Console.WriteLine($"\tBundle {update.FileName}");
        }
    }
}
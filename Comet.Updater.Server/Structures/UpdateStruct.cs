﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - UpdateStruct.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/16 15:47
// ===================================================================

#endregion

namespace Comet.Updater.Server.Structures
{
    public struct UpdateStruct
    {
        public int From;
        public int To;

        public string FileName => IsBundle ? $"{From}-{To}" : $"{From}";
        public string Extension;
        public string FullFileName => $"{FileName}.{Extension}";

        public bool IsBundle => To == 0 || From != To;
        /// <summary>
        /// If the current patch is a updater client patch.
        /// </summary>
        public bool IsUpdate => From >= 10000;
    }
}
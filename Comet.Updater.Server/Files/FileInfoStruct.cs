﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - FileInfoStruct.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/12 16:29
// ===================================================================

#endregion

#region References

using System.IO;

#endregion

namespace Comet.Updater.Server.Files
{
    public struct FileInfoStruct
    {
        public string FullPath => Path.Combine(Folder, FileName);
        public string Folder;
        public string FileName;
        public string Hash;
    }
}
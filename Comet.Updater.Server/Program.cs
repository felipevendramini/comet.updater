﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - Program.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/12 15:38
// ===================================================================

#endregion

#region References

using System;
using System.Threading.Tasks;
using Comet.Updater.Server.Configuration;
using Comet.Updater.Server.Structures;
using Comet.Updater.Server.Threads;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Server
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.Title = "Comet Updater Server is starting...";

            Console.WriteLine("Comet Updater Server");
            Console.WriteLine("Version: 1.0.0.0");
            Console.WriteLine();

            await Log.WriteLogAsync(LogLevel.Message, "Setting up folder listeners and caching information");

            Kernel.Config = UpdatesConfig.Create();

            var server = new Server();
            _ = server.StartAsync(Kernel.Config.ListenPort).ConfigureAwait(false);

            await Log.WriteLogAsync(LogLevel.Message, $"Listening on port {Kernel.Config.ListenPort:0000}");

            var backThread = new BackgroundThread();
            await backThread.StartAsync();

            await HandleCommandsAsync().ConfigureAwait(true);

            await backThread.CloseAsync();
        }

        static async Task HandleCommandsAsync()
        {
            await Log.WriteLogAsync(LogLevel.Message, "Server is ready! Commands: add, add-bundle, remove, list, cls");

            for (;;)
            {
                string command = Console.ReadLine();

                if (command == null)
                    continue;

                if (command.Equals("exit"))
                    break;

                switch (command)
                {
                    case "add":
                    {
                        Console.WriteLine("This command will add a new patch to the queue.");
                        Console.WriteLine("***REMINDER*** Updates above 10000 will be handled as primary updates that will force the updater client to restart.");
                        Console.WriteLine();
                        Console.Write("Please, type the number of the patch you want to add: ");

                        if (!int.TryParse(Console.ReadLine(), out var num))
                        {
                            Console.WriteLine("Invalid patch number.");
                            continue;
                        }

                        Console.Write("Please, type the file extension [7z, exe]: ");
                        string ext = Console.ReadLine();

                        if (!SharedKernel.IsSupportedExt(ext))
                        {
                            Console.WriteLine("Unsupported extension.");
                            continue;
                        }

                        if (Kernel.Config.ContainsUpdate(num))
                        {
                            Console.Write(
                                "The current patch is already on the queue. Do you want to override it? [Y/N]");
                            if (Console.ReadLine()?.Equals("Y") != true)
                                continue;
                            Kernel.Config.RemovePatch(num);
                        }

                        UpdateStruct update = new UpdateStruct
                        {
                            From = num,
                            Extension = ext,
                            To = num
                        };
                        if (Kernel.Config.AppendPatch(update))
                        {
                            await Log.WriteLogAsync(LogLevel.Message, $"Patch {num} has been added.");
                        }
                        else
                        {
                            await Log.WriteLogAsync(LogLevel.Error, $"Could not add patch {num}.");
                        }
                        break;
                    }

                    case "add-bundle":
                    {
                        Console.WriteLine("This command will add a new patch to the queue.");
                        Console.WriteLine("***REMINDER*** Updates above 10000 will be handled as primary updates that will force the updater client to restart.");
                        Console.WriteLine();
                        Console.Write("Please, type the number of the patch you want to add: ");

                        if (!int.TryParse(Console.ReadLine(), out var num))
                        {
                            Console.WriteLine("Invalid patch number.");
                            continue;
                        }

                        Console.Write("Please, type the final patch it'll update: ");
                        if (!int.TryParse(Console.ReadLine(), out var target))
                        {
                            Console.WriteLine("Invalid patch number.");
                            continue;
                        }

                        Console.Write("Please, type the file extension [7z, exe]: ");
                        string ext = Console.ReadLine();

                        if (!SharedKernel.IsSupportedExt(ext))
                        {
                            Console.WriteLine("Unsupported extension.");
                            continue;
                        }

                        if (Kernel.Config.ContainsUpdate(num))
                        {
                            Console.Write("The current patch is already on the queue. Do you want to override it? [Y/N]");
                            if (Console.ReadLine()?.Equals("Y") != true)
                                continue;
                            Kernel.Config.RemovePatch(num);
                        }

                        UpdateStruct update = new UpdateStruct
                        {
                            From = num,
                            Extension = ext,
                            To = target
                        };
                        if (Kernel.Config.AppendPatch(update))
                        {
                            await Log.WriteLogAsync(LogLevel.Message, $"Patch '{update.FileName}' has been added.");
                        }
                        else
                        {
                            await Log.WriteLogAsync(LogLevel.Error, $"Could not add patch {num}.");
                        }

                        break;
                    }

                    case "remove":
                    {
                        Console.Write("Write the patch FROM identity: ");
                        if (!int.TryParse(Console.ReadLine(), out var from))
                        {
                            Console.WriteLine("Invalid patch ID. Must be a number.");
                            continue;
                        }

                        if (!Kernel.Config.ContainsUpdate(from))
                        {
                            Console.WriteLine("Patch does not exist.");
                            continue;
                        }

                        if (Kernel.Config.RemovePatch(from))
                        {
                            Console.WriteLine($"Patch {from} removed.");
                        }
                        else
                        {
                            Console.WriteLine($"Could not remove patch {from}.");
                        }
                        Console.WriteLine();

                        break;
                    }

                    case "list":
                    {
                        Console.WriteLine();
                        Kernel.Config.ListUpdates();
                        Console.WriteLine();
                        break;
                    }

                    case "test":
                    {
                        Console.WriteLine("From what patch are you trying to update: ");
                        if (!int.TryParse(Console.ReadLine(), out var from))
                        {
                            Console.WriteLine("Value is not a Integer.");
                            continue;
                        }

                        Console.WriteLine();

                        Console.WriteLine("Updates list");
                        foreach (var patch in Kernel.Config.GetUpdateSequence(from, from < 10000))
                        {
                            Console.WriteLine($"\tFrom({patch.From:0000}), To({patch.To:0000}) -> {patch.FullFileName}");
                        }

                        Console.WriteLine();

                        break;
                    }

                    case "cls":
                    {
                        Console.Clear();
                        break;
                    }
                }
            }
        }
    }
}
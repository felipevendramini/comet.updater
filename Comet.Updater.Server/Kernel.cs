﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - Kernel.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/16 15:55
// ===================================================================

#endregion

#region References

using System.Collections.Concurrent;
using Comet.Updater.Server.Configuration;

#endregion

namespace Comet.Updater.Server
{
    public static class Kernel
    {
        public static UpdatesConfig Config;

        public static ConcurrentDictionary<int, Client> Clients = new ConcurrentDictionary<int, Client>();
    }
}
﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Server - BackgroundThread.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 19:13
// ===================================================================

#endregion

#region References

using System;
using System.Threading.Tasks;
using Comet.Updater.Shared;

#endregion

namespace Comet.Updater.Server.Threads
{
    public sealed class BackgroundThread : TimerBase
    {
        private const string TITLE_STR = "[Comet.Updater] Auto Updater Server - Latest Client({0:0000}) Latest Game Client Update({1:0000}) - {2}";

        public BackgroundThread()
            : base(1000, "Background Thread")
        {
        }

        protected override Task<bool> OnElapseAsync()
        {
            Console.Title = string.Format(TITLE_STR, Kernel.Config.LatestClientUpdate(), Kernel.Config.LatestGameClientUpdate(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            return Task.FromResult(true);
        }
    }
}
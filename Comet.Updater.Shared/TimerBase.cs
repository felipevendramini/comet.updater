﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Shared - TimerBase.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/17 18:57
// ===================================================================

#endregion

#region References

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

#endregion

namespace Comet.Updater.Shared
{
    public abstract class TimerBase
    {

        private CancellationToken mCancellationToken = CancellationToken.None;
        private Timer mTimer;
        private string mName;
        protected int mInterval = 1000;

        protected TimerBase(int intervalMs, string name)
        {
            mName = name;
            mInterval = intervalMs;

            mTimer = new Timer
            {
                Interval = intervalMs,
                AutoReset = false
            };
            mTimer.Elapsed += TimerOnElapse;
            mTimer.Disposed += TimerOnDisposed;
        }

        public string Name => mName;

        public long ElapsedMilliseconds { get; private set; }

        public async Task StartAsync()
        {
            await OnStartAsync();

            mTimer.Start();
        }

        public Task CloseAsync()
        {
            mTimer.Stop();
            mCancellationToken = new CancellationToken(true);
            return Task.CompletedTask;
        }

        private async void TimerOnDisposed(object sender, EventArgs e)
        {
            await OnCloseAsync();
        }

        private async void TimerOnElapse(object sender, ElapsedEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            while (!mCancellationToken.IsCancellationRequested)
            {
                sw.Start();
                try
                {
                    await OnElapseAsync();
                    await Task.Delay(mInterval, mCancellationToken);
                }
                catch (Exception ex)
                {
                    await Log.WriteLogAsync(LogLevel.Error, $"Error on thread {mName}");
                    await Log.WriteLogAsync(LogLevel.Exception, ex.ToString());
                }
                finally
                {
                    sw.Stop();
                    ElapsedMilliseconds = sw.ElapsedMilliseconds - mInterval;
                    sw.Reset();
                }
            }

            await Log.WriteLogAsync(LogLevel.Warning, $"Thread [{mName}] is stopping.");
        }


        protected virtual async Task OnStartAsync()
        {
            await Log.WriteLogAsync(LogLevel.Message, $"Timer [{mName}] has started");
        }

        protected virtual async Task<bool> OnElapseAsync()
        {
            await Log.WriteLogAsync(LogLevel.Message, $"Timer [{mName}] has elapsed at {DateTime.Now}");
            return true;
        }

        protected virtual async Task OnCloseAsync()
        {
            await Log.WriteLogAsync(LogLevel.Message, $"Timer [{mName}] has finished");
        }
    }
}
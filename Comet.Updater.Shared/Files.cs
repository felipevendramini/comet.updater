﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Shared - Files.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/12 16:26
// ===================================================================

#endregion

#region References

using System;
using System.IO;
using System.Security.Cryptography;

#endregion

namespace Comet.Updater.Shared
{
    public static class Files
    {
        public static string GetMd5(string path)
        {
            try
            {
                using var md5 = MD5.Create();
                using var stream = File.OpenRead(path);
                return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty).ToLowerInvariant();
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
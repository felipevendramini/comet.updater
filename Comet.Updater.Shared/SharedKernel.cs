﻿#region Credits and Copyright

// ===================================================================
// Comet Updater project - FTW Masters, Some Rights Reserved.
// 
// This project is open source and free and might not be sold.
// 
// Comet.Updater - Comet.Updater.Shared - SharedKernel.cs
// 
// File Creator: FELIPE VIEIRA VENDRAMINI
// Creation Date: 2021/03/16 15:45
// ===================================================================

#endregion

namespace Comet.Updater.Shared
{
    public static class SharedKernel
    {
        public static bool IsSupportedExt(string ext)
        {
            switch (ext.ToLowerInvariant())
            {
                case "7z":
                case "exe":
                    return true;
                default:
                    return false;
            }
        }
    }
}